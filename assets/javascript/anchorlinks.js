function addAnchor(element) {
  if (!element.classList.contains("no-anchor")) {
    element.classList.add("handbook-section");
    element.innerHTML = `${element.innerText} <a uk-icon="icon: link" class="handbook-anchor" href="#${element.id}"></a>`;
  }
}

document.addEventListener("DOMContentLoaded", function () {
  var headers = document.querySelectorAll("h2, h3, h4, h5, h6");
  if (headers) {
    headers.forEach(addAnchor);
  }
});
