!(function (h, t) {
  "object" == typeof exports && "undefined" != typeof module
    ? (module.exports = t())
    : "function" == typeof define && define.amd
    ? define("uikiticons", t)
    : (h.UIkitIcons = t());
})(this, function () {
  "use strict";
  let h = {
    "battery-full":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 14.5v-8h13v2h2v4h-2v2h-13z" stroke="#000"/><path fill="#000" d="M4 8h10v5H4z"/></svg>',
    "battery-half":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 14.5v-8h13v2h2v4h-2v2h-13z" stroke="#000"/><path fill="#000" d="M4 8h6v5H4z"/></svg>',
    "battery-low":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 14.5v-8h13v2h2v4h-2v2h-13z" stroke="#000"/><path fill="#000" d="M4 8h3v5H4z"/></svg>',
    battery:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 14.5v-8h13v2h2v4h-2v2h-13z" stroke="#000"/></svg>',
    bluetooth:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 13.125l9-6.875-4.846-3.75v15L15 13.75l-9-7.5" stroke="#000"/></svg>',
    bug: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M15.5 5.5h-11S2.197 16 10 16s5.5-10.5 5.5-10.5zM6.5 5.5s0-4 3.5-4 3.5 4 3.5 4M15.5 10.5h3M15.5 5.5l2-2M4.5 10.5h-3M4.5 5.5l-2-2M14.5 14.5l2 2M5.5 14.5l-2 2M7.5 8.5l5 5M12.5 8.5l-5 5"/></g></svg>',
    "chart-bar":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M2.5 2v15.5H18M8.5 16V4M5.5 16V9M11.5 16v-4M14.5 16V7"/></g></svg>',
    "chart-line":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M2.5 2v15.5H18"/><path d="M4.5 14.5l4-8 4 5 3-4"/></g></svg>',
    "chart-pie":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g fill="#000"><path d="M18 10a8 8 0 10-8 8v-8h8z"/><path d="M11 19a7.999 7.999 0 008-8h-8v8z"/></g></svg>',
    cloud:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)"><path d="M12.5 15.6l-8.75.01C1.96 15.61.5 14.17.5 12.39c0-1.63 1.22-2.98 2.81-3.19C3.38 6.31 5.75 4 8.68 4c2.51 0 4.63 1.71 5.21 4.02.5-.22 1.04-.34 1.61-.34 2.21 0 4 1.77 4 3.96s-1.79 3.96-4 3.96h-3z" stroke="#000" stroke-width="1.1"/></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h20v20H0z"/></clipPath></defs></svg>',
    "copy-clipboard":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6 12.5h5M6 8.5h4M6 6.5h7M6 10.5h4" stroke="#000"/><path d="M15 12.269L12.4 9.5 15 6.731 14.3 6 11 9.5l3.3 3.5.7-.731z" fill="#000"/><path d="M12 9.5h7" stroke="#000"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3.5 1H3v18h14v-5h-1v4H4V2h12v3h1V1H3.5z" fill="#000"/></svg>',
    dashboard:
      '<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 15h-1v2h1v-2zM14 15h-1v2h1v-2z" fill="#000"/><path d="M17 16H7v1h10v-1z" fill="#000"/><path d="M20.5 3.5h-17v11h17v-11z" stroke="#000"/><path fill="#000" d="M15 5h4v5h-4zM5 5h9v5H5zM11 15h-1v2h1v-2zM14 15h-1v2h1v-2z"/><path d="M17 16H7v1h10v-1z" fill="#000"/><path d="M20.5 3.5h-17v11h17v-11z" stroke="#000"/><path fill="#000" d="M5 11h4v2H5zM10 11h4v2h-4zM15 11h4v2h-4z"/></svg>',
    "eye-closed":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.97 11v2.5M6.47 10.5l-1 2M3.47 8.5L1.97 10M13.47 10.5l1 2M16.47 8.5l1.5 1.5" stroke="#000"/><path fill-rule="evenodd" clip-rule="evenodd" d="M2.519 6c.06.125.154.306.285.524a8.501 8.501 0 001.269 1.622C5.247 9.321 7.125 10.5 9.969 10.5s4.723-1.18 5.897-2.354a8.504 8.504 0 001.268-1.622c.131-.218.226-.4.286-.524h1.074l.006.02-.066.166L17.969 6l.464.186v.003l-.002.004-.006.015a3.189 3.189 0 01-.104.225 7.85 7.85 0 01-.33.606 9.502 9.502 0 01-1.418 1.815c-1.326 1.325-3.447 2.646-6.604 2.646-3.156 0-5.277-1.32-6.603-2.646a9.502 9.502 0 01-1.42-1.815 7.867 7.867 0 01-.41-.781 3.1 3.1 0 01-.022-.05l-.006-.015-.002-.004v-.002L1.968 6l-.464.186-.066-.166.006-.02H2.52z" fill="#000"/></svg>',
    eye: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 6.5c-6 0-8 5-8 5s3 5 8 5 8-5 8-5-2-5-8-5z" stroke="#000"/><circle cx="10" cy="11.5" r="2" fill="#000"/><path d="M10 6.5V4M13.5 7l1-2M16.5 9L18 7.5M6.5 7l-1-2M3.5 9L2 7.5" stroke="#000"/></svg>',
    "folder-open":
      '<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.5 17V4.5h7l1 2h9v3" stroke="#000"/><path fill-rule="evenodd" clip-rule="evenodd" d="M4.5 8.921h-.333l-.128.307-3 7.158-.039.092v.6h17.833l.128-.306 3-7.158.29-.693H4.5zM2.252 16.08l2.58-6.158h15.916l-2.58 6.158H2.251z" fill="#000"/></svg>',
    folders:
      '<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M5.5 6.5v12h17v-10h-9l-1-2h-7z"/><path d="M4 13.5H1.5v-12h7l1 2h9V7"/></g></svg>',
    fullscreen:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M2.5 12v5.5H8M12 17.5h5.5V12"/><g><path d="M2.5 12v5.5H8M8 2.5H2.5V8M12 17.5h5.5V12M17.5 8V2.5H12"/></g></g></svg>',
    gitlab:
      '<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.2503 8.43663L19.2292 8.38068L17.1876 2.8489C17.1461 2.74048 17.0725 2.6485 16.9775 2.58618C16.8824 2.5249 16.7716 2.49539 16.6598 2.50163C16.5481 2.50786 16.4409 2.54955 16.3527 2.62104C16.2655 2.69459 16.2022 2.79426 16.1715 2.90647L14.793 7.28519H9.21093L7.83241 2.90647C7.80251 2.79365 7.73911 2.69348 7.65121 2.62023C7.56302 2.54873 7.45581 2.50705 7.34408 2.50082C7.23234 2.49458 7.12145 2.52409 7.02639 2.58536C6.93159 2.64795 6.8581 2.73984 6.81629 2.84809L4.77077 8.37744L4.75046 8.43339C4.45656 9.23065 4.42028 10.1055 4.6471 10.926C4.87391 11.7466 5.35152 12.4683 6.00792 12.9824L6.01495 12.9881L6.03369 13.0019L9.14376 15.4199L10.6824 16.6289L11.6196 17.3635C11.7293 17.45 11.8631 17.4967 12.0008 17.4967C12.1384 17.4967 12.2723 17.45 12.3819 17.3635L13.3192 16.6289L14.8578 15.4199L17.9866 12.9873L17.9944 12.9808C18.6493 12.4666 19.1258 11.7456 19.3523 10.9262C19.5788 10.1067 19.543 9.23313 19.2503 8.43663V8.43663Z" fill="#6F6F6F"/><path d="M19.2503 8.4366L19.2292 8.38065C18.2344 8.59264 17.2969 9.03012 16.4838 9.66183L11.9999 13.1818C13.5269 14.3811 14.8562 15.4231 14.8562 15.4231L17.985 12.9905L17.9928 12.984C18.6487 12.4698 19.1259 11.7483 19.3527 10.9282C19.5795 10.1081 19.5435 9.23368 19.2503 8.4366Z" fill="#909090"/><path d="M9.14383 15.4231L10.6825 16.6321L11.6197 17.3668C11.7293 17.4532 11.8632 17.5 12.0008 17.5C12.1385 17.5 12.2724 17.4532 12.382 17.3668L13.3192 16.6321L14.8579 15.4231C14.8579 15.4231 13.527 14.3779 12.0001 13.1819C10.4731 14.3779 9.14383 15.4231 9.14383 15.4231Z" fill="#AFAFAF"/><path d="M7.51531 9.66184C6.70288 9.02883 5.76565 8.59021 4.77077 8.37741L4.75046 8.43336C4.45656 9.23062 4.42028 10.1055 4.6471 10.926C4.87391 11.7465 5.35152 12.4683 6.00792 12.9824L6.01495 12.988L6.03369 13.0018L9.14376 15.4199C9.14376 15.4199 10.4715 14.3779 12 13.1786L7.51531 9.66184Z" fill="#909090"/></svg>',
    incognito:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.9 10.8a4.4 4.4 0 100-8.8 4.4 4.4 0 000 8.8zM1.5 19c.8-4.5 4.3-7.8 8.5-7.8s7.7 3.4 8.5 8" stroke="#000" stroke-width="1.1"/><mask id="a" fill="#fff"><path d="M10.5 19a.5.5 0 100-1 .5.5 0 000 1z"/></mask><path d="M10.5 19a.5.5 0 100-1 .5.5 0 000 1z" fill="#000"/><path d="M10 18.5a.5.5 0 01.5-.5v2a1.5 1.5 0 001.5-1.5h-2zm.5-.5a.5.5 0 01.5.5H9a1.5 1.5 0 001.5 1.5v-2zm.5.5a.5.5 0 01-.5.5v-2A1.5 1.5 0 009 18.5h2zm-.5.5a.5.5 0 01-.5-.5h2a1.5 1.5 0 00-1.5-1.5v2z" fill="#000" mask="url(#a)"/><path d="M9 14.468c0-1.943 3-1.956 3-.045 0 .608-.6.915-.982 1.298-.317.314-.475.627-.515 1.055-.006.083-.013.154-.013.224" stroke="#000" stroke-width="1.2"/></svg>',
    npm: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3 17V3h14v14H3z" stroke="#000" stroke-width="2"/><path fill="#000" d="M11 7h2v10h-2z"/></svg>',
    save: '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill="#000" d="M12 2h1v3h-1z"/><path d="M15.885 1.5H1.5v17h17v-14l-2.615-3z" stroke="#000"/><path d="M5.5 1.5v4h8v-4M4.5 18.5v-10h11v10M7 11.5h6M7 13.5h6M7 15.5h6" stroke="#000"/><path fill="#000" d="M6 2h4v3H6z"/></svg>',
    "sort-ascending":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.269 12L4.5 14.6 1.731 12 1 12.7 4.5 16 8 12.7l-.731-.7z" fill="#000"/><path d="M4.5 15V3M9 15.5h10M9 12.5h8M9 6.5h4M9 3.5h2M9 9.5h6" stroke="#000"/></svg>',
    "sort-descending":
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.731 12L4.5 14.6 7.269 12l.731.7L4.5 16 1 12.7l.731-.7z" fill="#000"/><path d="M4.5 15V3M9 3.5h10M9 6.5h8M9 12.5h4M9 15.5h2M9 9.5h6" stroke="#000"/></svg>',
    terminal:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M2 2l7.778 7.778L2 17.556M10 17.5h9"/></g></svg>',
    usb: '<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill="#000" d="M15 13h3v3h-3z"/><path d="M4.8 10H22M7.8 10l2.7-4.5H14M10.2 10l2.3 4.5h4.8" stroke="#000"/><circle cx="3.5" cy="10" r="2.5" fill="#000"/><circle cx="13.5" cy="5.5" r="1.5" fill="#000"/><path d="M23 10l-3.75 3.031V6.97L23 10z" fill="#000"/></svg>',
    windowed:
      '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><g stroke="#000"><path d="M7.5 18v-5.5H2M2 7.5h5.5V2M18 12.5h-5.5V18M12.5 2v5.5H18"/></g></svg>',
  };
  function t(l) {
    t.installed || l.icon.add(h);
  }
  return "undefined" != typeof window && window.UIkit && window.UIkit.use(t), t;
});
