---
title: Investors & fundraising
description:
  This page describes how we work with our investors, as well as links to
  information that our investors might find useful.
---

# {{% param "title" %}}

{{% param "description" %}}

## Elevator pitch

Our public mission and business overview can be found on our
[direction page](../../general/direction/). We also have a
[business one-pager](https://docs.google.com/document/d/1Wf2n7pi1DcNU1ktYQnc26rE0g9CPU9UfSezwwz9MWLc/edit#)
(internal only), which contains a more summarized version of the same
information.

## Investor agenda

## Biweekly team and investor Call

We have an recurring biweekly calls for the entire team to meet with our
investor and discuss different objectives for the business.

Here are "norms" for the call:

- Prior to the call, please add a discussion topic or update to the meeting
  [agenda](https://docs.google.com/document/d/11_bxolgHm-lpp1XQhSd0GaEI1Alvkk-KcZcS4hElcGE/edit#).
  Prefix the question with your name.
- In the call, we will go through them in order, giving each person the chance
  to voice their updates.
- Note that this call is joined by GitLab's "CEO Shadows" -- please see more
  about this program here:
  [https://about.gitlab.com/handbook/ceo/shadow/](https://about.gitlab.com/handbook/ceo/shadow/)

## Investor materials

Answers to common investor questions are still TBD but will be available in
Google Docs once created. (team members only).

## Product demo

A product demo and demo script is TBD but will be created soon.
