---
title: Contact information
description:
  This page contains important contact details for the company such as our
  mailing address and public inquiry email address.
---

# {{% param "title" %}}

{{% param "description" %}}

## Email inquiries

For email inquiries use contact@synura.com.

## Company mailing address

We have a mailing address through
[Earth Class Mail](https://www.earthclassmail.com/). This service scans our mail
and sends it to us as an email attachment. Here is our address:

```text
Synura, Inc.
548 Market St
PMB 25791
San Francisco, California 94104-5401 US
```

Please use our dedicated mailbox for all accounts requiring a **mailing
address** or **billing address**:

Note: the company does not have any method of receiving packages or physical
items.
