---
title: Finance
description:
  Our financial management procedures and policies, including how we track
  assets, spend and track money, deal with invoices, and so on.
---

# {{% param "title" %}}

{{% param "description" %}}

## Fixed assets tracking & management

1. Fixed assets such as office furniture, computer equipment, laptops, are
   purchased for long-term use. They usually have a useful life of more than one
   year. Synura tracks its fixed assets with a unit price over $500 USD via
   [this document](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0).
2. The purchaser of the assets is required to log in to this file to fill out
   the information (as indicated in the file), such as date of purchase,
   purchaser's name, the value of the asset, etc.
3. Any team members who received the company purchased laptops or computer
   equipment are required to log in to this file and fill out the asset
   description, model #, and serial #.
4. Please make sure the information of the Fixed Asset Tracking file is
   completed _within 7 business days_ of the purchase date when the asset is
   initially purchased.
5. If a transition happens from existing computer equipment to another team
   member, the recipient of the computer equipment is required to acknowledge
   the receipt of the computer equipment _within 7 business days_. Please log in
   to the Fixed Asset Tracking file and fill out the required information (as
   indicated in the file), such as the description of the asset transition, date
   of transition, old owner, new owner, etc.

### Purchasing company assets that are no longer used for business

If an employee is interested in buying a company asset that is no longer used
for business, the CEO will review case by case, with consideration to whether an
asset can be used again, value to the company, and define a fair market value
for sell. CEO should notify Accounting immediately if a decision is made to
allow an employee to purchase the company's assets, so Accounting can make sure
this is recorded properly.

## Invoice submissions

1. All invoices are required to submit to accounting@synura.com.
2. Please DO NOT submit invoices and/or payment instructions via GitLab/Notion,
   etc.
3. Vendors are required to submit their invoices within 7 days after the
   services are rendered or the goods are delivered. Timely submission of your
   invoices will enable Accounting to record the expenses in the right financial
   period.

### Important tax form requirements

1. For **US-based vendors**, vendors are required to provide a completed
   [W-9 Form](https://www.irs.gov/pub/irs-pdf/fw9.pdf) to [insert group
   accounting email] prior to or at the time when they submit their 1st invoice.
2. **For Non-US vendors**, vendors are required to provide a completed
   [W-8BEN-E Form](https://www.irs.gov/pub/irs-pdf/fw8bene.pdf) prior to or at
   the time they submit their 1st invoice to accounting@synura.com. Here are the
   [instructions](https://www.irs.gov/instructions/iw8bene) for Form W-8BEN-E
   Form.
3. Important: Accounting cannot process a payment without these Tax Forms

## Invoice approvals

1. All invoices that require Accounting to make a payment are required to be
   processed in Bill.com
2. Approvals of invoices are completed in Bill.com.
3. Accounting will route the invoices according to the following approval
   hierarchy
4. In general, invoices up to $5000 can either be approved by CTO or CEO.
5. Invoices over $5000 require CEO approval
6. **Project Related Invoice Approval**. For Project-related invoices, ONLY the
   Head of Engineering team approves the bill in Bill.com. The CTO and the CEO
   can approve the payment in SVB for wires.
7. The Head of Engineering and Accounting can review and validate the spending
   according to the Project budget periodically. However, all Project-related
   bills will need the Head of Engineering to approve in Bill.com to enable
   proper internal control.

## Invoice receiving acknowledgement and documentations (Accounting’s responsibilities)

1. Process invoices received in accounting@synura.com in Bill.com and route the
   invoices for approval in Bill.com according to the Invoice Approval
   Hierarchy.
2. The AP accountant will reply to accounting@synura.com and the vendor to
   acknowledge that the invoice has been processed in Bill.com. The turnaround
   time is usually 1-2 business days from the day the invoice is submitted to
   Accounting and the acknowledgment.
3. AP Accountant needs to make sure to use the contractor's contact email to
   send out bill.com e-invite (do not use the contractor's Synura email). This
   will help us to make sure the applicable contractors can receive their tax
   forms even if they are no longer working with Synura
4. For US vendors, upload a copy of W-9 in Bill.com under the Document section
5. For non-US vendors, upload a copy of
   [W-8BEN Form](https://www.irs.gov/pub/irs-pdf/fw8ben.pdf) in Bill.com under
   the Document section

## Invoice payments

### Bill.com payments

1. Once an invoice is approved, payment is usually processed based on Net 30
   payment terms unless otherwise specified in an agreement or invoice
2. Bill.com e-payment is recommended to pay all vendors
3. To establish Bill.com e-payment, Accounting will send an e-invite to an email
   that is provided by the vendor. The vendor accepts the invite and provides
   their bank information within Bill.com. The payment will be scheduled to
   deposit to the vendor's account according to the payment terms. E-payment
   will usually arrive in the recipient's account within 2-4 business days.

**Exception:** For some non-US vendors who may have difficulties setting up a
Bill.com e-payment, a wire payment will be processed.

### Wire payments

All wires from SVB are initiated by Accounting and approved by Synura authorized
SVB signer(s) per the authorization that is set up with SVB.

#### Wire requestor's responsibilities

1. Make sure the vendor provides all the required information as outlined below
   before requesting a wire payment
2. For a new vendor's 1st payment, please send an email to [insert group
   accounting email] with invoices, wire instructions, Form W-8BEN-E (if non-US
   vendors). For the 2nd payment and onward, send invoice only is okay unless
   the vendor requests any changes, such as bank changes
3. Make sure the vendor provides a formal invoice. (A quotation and/or a Pro
   Forma Invoice cannot be accepted to issue a payment) Exceptions should be
   discussed with the CTO or CEO and Accounting.
4. For non-US vendors, make sure the invoices indicate the proper currency Wire
   payment is used to pay any vendors who cannot get paid via Bill.com or to
   provides funds to **non-US team members** for business expenses. To request a
   wire payment, please send an email request to [insert group accounting email]
   along with:
5. The invoice to be paid
6. Vendor's wire instructions - see below-requested information about wire
   instructions
7. If a foreign currency wire payment is required, please indicate which
   currency
8. If a wire payment needs to arrive by a certain date, please be very specific
   about it so Accounting can plan accordingly
9. A brief description for the Purpose of Payment (per bank requirement for
   international wires)
10. **Bank Fees** If any bank fees that Synura should be covering to ensure the
    recipient can receive the full payment, please notify Accounting about how
    much bank fees to add to the wire total as the fees can vary vendor by
    vendor. For example, some Zambia vendors don't need Synura to include any
    bank transfer charges, some of them do. Given each vendor's situation is
    different, Accounting requests vendors to provide how much bank fee to
    include and in what currency with the 1st payment request. For future
    recurring payments, Accounting can include the same amount of bank fees
    without asking again unless new changes are provided by the vendor and the
    amount of change is justified by Accounting.

Typically, a wire instruction needs to include:

1. Recipient's name
2. Recipient's address
3. Recipient's bank name
4. Recipient's bank address
5. Recipient's bank account
6. Recipient's bank ABA Routing Numbers (if domestic wire within the United
   States)
7. Recipient's IBAN, BIC, or SWIFT Code (if international bank outside of United
   States)

Sometimes the bank may require to provide additional information, Accounting
will reach out to the wire requestor if such request arises.

1. Wire payment is usually quicker, but for international wire payment, it still
   takes a few business days to clear. We suggest you submit the wire request at
   least 7 business days prior to the payment due date to allow accounting to
   get the wire initiated and approved.
2. If the wire payment is urgent, please flag "**URGENT Payment Request**" in
   the subject line when you send the wire request to accounting@synura.com.
   Please don't submit a payment request via a Gitlab ticket as it can easily be
   buried in the thread.
3. If the wire payment is recurring, please send the invoice that needs to be
   paid to Accounting and indicates this is a recurring payment with no wire
   instruction change. Please be specific if the wire needs to arrive by a
   certain date so Accounting can plan the wire accordingly.

Once a wire payment is initiated and approved, the controller will send a proof
of payment to the wire requestor in the following business day or when such
document becomes available in SVB. When the controller is not available, such as
on vacation, accounting@synura.com will provide such proof of payment.

### Payroll

1. Payroll for US-based full-time employees is managed via Gusto.
2. Synura runs a semi-monthly payroll schedule.
3. US Employees will complete the onboarding process through Gusto and set up
   direct deposits for their payroll payments.

### Contracted non-US team members

1. For contracted **non-US team members**, a completed W-8BEN Form is collected
   during the [Pilot](#pilot) onboarding process.
2. Payments related to the contracted services are usually processed in
   [Pilot](#pilot) monthly.
3. Expense reimbursement reports from the **non-US team members** are required
   to submit in Expensify.

Please refer to the Expense Reports and Pilot sections below for more details.

### Pilot

Synura uses [Pilot](https://pilot.co) to process payments for **non-US team
members**.

When a team member successfully completes the onboarding process, the direct
manager of the new hire should notify Accounting immediately via sending an
email to accounting@synura.com with the information below:

- Full name of the new hire
- Official start date
- Annual or monthly compensation in USD$

With the new hire's information, Accounting will help to pre-generate the
monthly invoices for the next 12 months on behalf of the new hire. If there are
any exceptions on this, please notify Accounting by sending an email to
accounting@synura.com.

Since the team members in Pilot are mostly based outside of the US and the
processing time of non-US payment varies, Accounting has developed a monthly
payment calendar to streamline the payment schedule and to allow the funds to
arrive on or before the last day of each month.

For new hires who joined on or before the 22nd of a given month, Accounting will
try to include the new hire in the monthly payment schedule. If a new hire
joined after the 22nd of a given month, Accounting may need to run an off-cycle
payment for the new hire. Accounting will notify the new hire when the payment
will arrive in the situation of an off-cycle payment.

If you have any questions or concerns about the status of your payment, please
contact accounting@synura.com.
