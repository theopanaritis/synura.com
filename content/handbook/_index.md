---
title: Handbook
description:
  Welcome to the public handbook for Synura, which includes information on the
  company, how we work, benefits, and more.
---

# {{% param "title" %}}

{{% param "description" %}}

Our handbook is public for a few reasons. The first is that it gives our
customers insights into how we work together, and allows anyone to give us
feedback (and even propose changes) that help us make a better product. The
second is that it gives a chance for potential team members to learn a lot about
what their day to day will look like, helping them make a better informed
decision about joining us.

The content here is organized into a few sections:

## General

- [Direction](general/direction/)
- [Values](general/values/)
- [Product Development](general/product_development/)
- [Communication](general/communication/)
- [Marketing & brand](general/marketing/)
- [Meetings](general/meetings/)
- [Tools](general/tools/)

## Corporate

- [Contact information](corporate/contact/)
- [Finance](corporate/finance/)
- [Investors & fundraising](corporate/investors/)
- [Legal & general business](corporate/legal/)
- [Privacy policy](corporate/privacy-policy/)

## People

- [Offboarding](people/offboarding/)
- [Onboarding](people/onboarding/)
- [PeopleOps](people/peopleops/)
- [Performance metrics & evaluation](people/performance/)
- [Recruiting](people/recruiting/)
- [Travel](people/travel/)

## Benefits

- [Benefits overview](benefits/overview/)
- [Holidays & PTO](benefits/holidays_pto/)
- [Training & development](benefits/training_dev/)

## Readmes

- [Jason Yavorska](readmes/jason.yavorska/)
