---
title: Holidays & PTO
description:
  This page describes how holidays and PTO work, as well as details on how to
  properly submit and track your time off.
---

# {{% param "title" %}}

{{% param "description" %}}

## Paid time off

Synura offers all team members unlimited time off. Whether you want to take a
trip, you are eager for some time to relax, or you need to get some chores done
around the house, any reason is a good reason for personal time. For team
members working in jurisdictions that require certain mandatory sick leave or
PTO policies, Synura complies to the extent required by law.

## Taking time off

When you take any time off, you should follow this process:

- Let your manager and team know as soon as possible. (e.g., post a message in
  your team's Slack channel)
- Find someone to cover anything that needs covering while you're out. (i.e.,
  meetings, planned tasks, unfinished business, important Slack/email threads,
  anything where someone might be depending on you)
- Mark an all-day "out of office" event in Google Calendar for the day(s) you're
  taking off.

This process is the same for any days you take off, regardless of whether it's a
holiday or you just need a break.

## Holidays

At Synura, we have team members with various employment classifications in many
different countries around the world. Synura is a US company, but we think you
should choose the days you want to work and what days you are on holiday, rather
than being locked into any particular nation or culture's expectation about when
you should take time off.

When a team member joins Synura, they pick one of the following holiday
schedules:

- Based on the country where you work. Non-US team members should let their
  managers know the dates of national holidays. or
- No set schedule, start with no holidays and add the days that are holidays to
  you.

Either way, it's up to you to ensure that your responsibilities are covered, and
your team knows that you're out of office.

### Winter holiday week off

Starting on December 24th, Synura enjoys a company winter holiday week. We close
our offices for the week so people can take this time to rest and celebrate the
holidays, and focus on their loved ones.

Please remember to update PTO by Roots and select Winter Holiday Week.

For Support during the holiday, please refer to the on-call schedule (document
TBD).

### End of year celebration

The end of year is a natural time for celebration and togetherness. For many
companies, an "end of year party" is a time to be together with coworkers and to
share the experience with significant others. We also want these qualities, but
as an all-remote company, take a different approach.

We ask that each team member have a special celebratory dinner with their family
or close friends, courtesy of Synura, and then share pictures and stories from
the experience with us in the #celebrations Slack channel. We hope this will
help each person enjoy the festive spirit with their loved ones, and for us all
to get to know one another better through the stories.

### Spend the day with your Family and Friends

At Synura, we value family and friends. In an ongoing pandemic with COVID-19
affecting many of our team members, we want to ensure that people are making
their well-being a top priority and that we are living out our values, to
emphasize this we will coordinate a Quarterly "Family and Friends Day" for as
long as the majority of the world, where our team members reside, are dealing
with COVID-19. On this day, we will close the doors to the virtual office,
reschedule all meetings, and have a **publicly visible shutdown**.

Team members can share about their Family and Friends Day in the Family &
Friends day Slack channel after the event, or publicly on social media such as
Twitter, LinkedIn, or wherever they're most comfortable using the hashtag
`#FamilyFriends1st`. Sharing is optional. Taking the day off is strongly
encouraged if your role allows it.

#### Upcoming Family and Friends Days

1. TBD

We will look at scheduling future dates roughly quarterly. In line with our Paid
Time Off policy, we encourage Synura Team Members to continue to take additional
days off as needed. Family and Friends Day is a reminder to do this.

#### FAQ about Family and Friends Day

##### Who determines upcoming Family and Friends Days?

Any Synura team member is able to propose a Family and Friends Day. To propose a
Family and Friends Day please follow the steps outlined below:

1. Review the Synura Team Meetings calendar for major conflicts, review major
   holidays, and avoid scheduled Investor calls and Customer Council Meetings.
2. Submit a request to peopleops@synura.com

##### I'm in a role which requires me to work that day

If you are in a role that requires you to work on Family and Friends Day, you
can work with your manager to find an alternative day. We encourage you to
consider the following business day as the preferred second choice for a day
away, but do what works best for you and your team.

##### What if the date is a public holiday or non-working day in my country?

We encourage you to take off the next working day. If this day isn't an option,
work with your manager to find another day that works for you and your team.

##### How is this any different than our vacation policy?

Nothing about our Paid Time Off policy is changing. We wanted to designate a
specific day in order to more proactively force a pause for team members. If
most of the company isn't working, there is less pressure for you to do so.

##### What about client or prospect meetings that conflict?

If you feel that this meeting can be rescheduled without any setbacks to the
business, please go ahead and do so. If you have a meeting that would be hard to
reschedule or would jeopardize the business results, please work with your
manager to find another day that would work for both you and your team.

##### What if I'm out sick on either of those days?

Feel better! Please work with your manager to find another day that works for
you and your team.

##### How do I communicate that I'm off that day?

We'll assume that most people are off on Family and Friends Day, but we know
that some people will take other days.

Please update PTO by Roots in Slack. You can select Create an OOO Event and find
Family and Friends Day in the drop-down menu of What type of OOO is this?.

Feel free to block your calendar with "Family and Friends Day" to share whatever
day you take.

## Work availability and paid time off

We emulate GitLab’s “don’t ask, must tell” policy, and encourage people to take
the time off. It's up to each teammate to choose what days will work best for
them, but we generally encourage you to take your national Holidays, in addition
to some long weekends and vacations.

References that may be helpful:

- [GitLab's Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/)
- [GitLab's Guidance on Mental Health](https://about.gitlab.com/company/culture/all-remote/mental-health/)

### How to take time off

You can take time off whenever you want. However, please be responsible in when
you take it and how you communicate it. Here are a few things to consider:

1. Check with your manager to try to avoid being out around a critical milestone
   or at the same time that others on your team will be out
2. Try to provide advance notice -- a general guideline would be to share twice
   as many days in advance as you'd like to take off. e.g. two days' notice is
   for one day off; two weeks in advance would be better if you want to take a
   full week.
   - Note that this does not apply for emergencies, and is not a requirement.
     Sometimes fun things come up last minute that you want to do. But this
     guideline will help prevent leaving others in the lurch when you have some
     flexibility in scheduling.
3. Team members who are with us through an agency should also follow that
   agency's procedures in addition to ours.
4. If you will miss the sprint video update, please record one in advance so
   that others will be aware of what you contribution during the sprint
5. Being part of a global remote team means you need to be highly organized and
   a considerate team player. Each team has busy times so it is always a good
   idea to check with them to ensure there is adequate coverage in place.
6. Engineers should ensure the following are in place before going on leave:
   - All tickets assigned to you are completed, verified and closed
     - If there are pending tickets, discuss with an engineer who can take up
       while you are gone
   - There are no merged requests assigned to you that are not merged
   - If you are taking a longer leave, communicate with the team few days prior
     to your leave

### How to add bulk time off

We encourage you to take the national holidays in your home country. We've made
this easy by providing a calendar with those dates so that you can add them in
bulk.

1. Visit our important dates spreadsheet (document TBD) and see the tab for your
   National Holidays
2. Create a local CSV file of that tab, using the menu option File: Download:
   CSV.
   - After you create a copy, feel free to add or remove dates as fit to your
     circumstances
   - Note that holidays that occur on weekdays have start and end times, so that
     importing them will block off your calendar
   - [More instructions](https://support.google.com/calendar/answer/37118?co=GENIE.Platform%3DDesktop&hl=en)
     are available from Google
3. Import into your Google Calendar using its Settings: Import & export feature
4. Confirm it added the dates properly

Note that you should notify your manager when you opt which dates to take and if
you made an deviations from these defaults. It's not necessary to email all@ if
you use these default dates.

## Absenteeism

Our time off policy is flexible, but requires proactive communication. Any team
member who takes 3 consecutive days without prior notification will be
considered to have voluntarily resigned.

If someone is absent, the manager will make effort to contact the team member or
someone listed as their emergency contact. Please share emergency contact info
(document TBD) with people-ops, so that we know who to call in case of an
unexpected absence.

Managers can find information on who to call in our emergency contact info sheet
(document TBD).

## Communicating your time off

Once you've decided to take time off and your manager is aware, it should be
communicated to the rest of the team. Here's how:

1. Mark the time as Out of office on your calendar.
   1. Select the Automatically decline new and existing meetings option so that
      your meeting status will show "declined"
2. You should add the time off using PTO by Roots to Slack. Please use the
   following syntax which uses the
   [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date notation in order to
   avoid confusion: OOO from YYYY-MM-DD to YYYY-MM-DD, please contact XXX for
   assistance.
3. Email [insert group email] with the dates you will be away (this is not
   necessary for National holidays planned in advance; see below for that
   process)
4. If you won't check email at all, add an out-of-office response in your email
   settings with the start and end date of your time off so that people reaching
   out to you on email will be notified
5. It may be useful to share your planned time off as a **FYI** on your team's
   Slack channel(s), especially if you lead a team.

## PTO by Roots

[PTO by Roots](https://www.tryroots.io/pto) allows employees and managers to
coordinate time off seamlessly with intuitive Slack commands. The integration
from Slack automatically monitors PTO balances and takes action. PTO by Roots
also reminds employees of their upcoming time off and helps them assign roles
and tasks for co-workers, giving all parties involved greater alignment and
peace of mind.

[PTO by Roots Training Video](https://drive.google.com/file/d/1t-rRHvWxo9IguvtHuacR0pAlK3nHWS4H/view?usp=sharing)

Please be aware that new team members' PTO by Roots accounts are activated and
synced automatically once a day, so if you try to access PTO by Roots on your
first day and receive an error message, please attempt to access PTO by Roots on
Day 2. If you still receive an error message, let your assigned People Ops
Specialist know in your onboarding issue.

If you receive an error message **"Uh-oh! It doesn't look like you have access
to PTO by Roots"** please attempt the following steps.

1. Check to see if your email address (Synura) in your slack profile matches
   your email address.
2. If these two are not the same exact email then the application will not
   recognize you as a team member.
3. If these two are the same, please reach out in the #peopleops channel.

### Slack commands

- /pto-roots ooo Create an OOO event.
- /pto-roots me View your OOO dashboard to edit, add or remove OOO events.
- /pto-roots whosout See upcoming OOO for everyone in the channel where this
  command is used.
- /pto-roots @username Check if a particular person is OOO and if they are, see
  which of your co-workers are covering for them.
- /pto-roots feedback This is your direct line to support. At any time, use this
  command to report bugs or share your thoughts on how the product can be
  improved or what’s working well.
- /pto-roots help A top-level look at PTO by Roots items that you may need help
  with, and how to receive help.
- /pto-roots settings This is where you modify your profile and calendar
  settings. This is also where you opt-in and out-put for reminders, including
  monthly messages prompting you to consider what PTO you may take this month.

### Additional features

### Google Calendar sync

PTO by Roots allows you to sync time off requests with your Google Calendar.

### Automatic status + do not disturb updates

PTO by Roots can automatically set your OOO status in Slack and apply “Do Not
Disturb” settings while you’re away. You must add these permissions
individually.

### Roles and task handoffs

PTO by Roots provides an experience that allows you to set roles and tasks for
co-workers while you’re away. Accepted roles then become a part of a Slack away
message that appears as co-workers try to tag you in discussions while you’re
OOO.

### Bulk add holidays

You can bulk add holidays based on your region and then modify them manually if
needed. Any Public Holidays added to PTO by Roots in your dashboard will not
auto-set an OOO event for you, you will still need to create an OOO event if
observing that holiday. To add a Public Holiday, follow the below instructions.

- Type /pto-roots me in Slack
- Choose Holidays from the Upcoming OOO dropdown menu
- Choose the desired year
- Click on Bulk Add By Region

## Parental leave

Synura offers anyone (regardless of gender) who has been at Synura for six
months up to 16 weeks of 100% paid time off during the first year of parenthood.
This includes anyone who becomes a parent through childbirth or adoption. The
paid time off is per birth or adoption event, and may be used only within the
first 12 months of the event.

We encourage parents to take the time they need. Synura team-members will be
encouraged to decide for themselves the appropriate amount of time to take and
how to take it.

### New parent leave

Synura gives new parents six weeks paid leave. After six weeks, if you don't
feel ready to return yet, we'll set up a quick call to discuss and work together
to come up with a plan to help you return to work gradually or when you're
ready.

### How to initiate Your parental leave

Some teams require more time to put a plan of action in place so we recommend
communicating your plan to your manager at least 3 months before your leave
starts. In the meantime, familiarize yourself with the steps below:

To initiate your parental leave, submit your time off by selecting the Parental
Leave category in PTO by Roots at least 30 days before your leave starts. We
understand that parental leave dates may change. You can edit your PTO by Roots
at a later time if you need to adjust the dates of your parental leave. It's
important that you submit a tentative date at least 30 days in advance. Your
manager will get notified after you submit your leave and will send you an
e-mail within 48 hours confirming that they've been notified of your Parental
Leave dates.

Please note, even though we have a "no ask, must tell" Paid Time Off Policy,
it's important that your Team Head is aware of your leave at least 30 days
before your leave starts.

### Planning Your parental leave dates

Your 16 weeks of parental leave starts on the first day that you take off. This
day can be in advance of the day that the baby arrives. You don't have to take
your parental leave in one continuous period, we encourage you to plan and
arrange your Parental Leave in a way that suits you and your family's needs. You
may split your Parental Leave dates as you see fit, so long as it is within the
12 months of the birth or adoption event.

If you don't meet the initial requirements for paid leave, Synura payroll
coverage begins once you meet the requirements. Until then, you will not receive
pay. If for example, you are someone who qualifies after 6 months at Synura and
then goes on leave at the start of your 120th day at Synura, you would not
receive payment from Synura for the first 60 days. You would receive payment
from Synura for up to 60 additional days taken within a year from the birth
event.

You can change the dates of your parental leave via PTO by Roots. Your Team Head
will receive a notification every time you edit your Parental Leave dates. Make
sure your leave is under the Parental Leave category, otherwise your Team Head
won't get a notification.

Please note, if you are planning to change or extend your Parental Leave by
using a different type of leave such as PTO, unpaid leave or any local statutory
leave, please send an e-mail to your Team Head.

### Administration of parental leave requests

For Your Team Head:

- PTO by Roots will notify Your Team Head of any upcoming parental leave.
- Notify the team member that the parental leave request was received by sending
  a confirmation e-mail. .
- PTO by Roots will automatically update the employment status.
- Check if the team member has confirmed their return via email. If not, send
  the Return to Work e-mail to the team member and manager.
- Do a weekly audit of parental leave return dates. If a team member hasn't sent
  an e-mail within 3 days of returning, send them the return to work e-mail,
  copying the manager in.
