---
title: Values
description:
  A shared set of values is important for how a company conducts itself, and
  we're no different. Learn about the values that set us apart here.
---

# {{% param "title" %}}

{{% param "description" %}}

## Iteration

By keeping our “build & learn” loop tight, we can consistently deliver value and
adapt to how our users respond to them. Through building iteratively we avoid
making things more complex than they need to be. Small steps forward,
consistently made, is how we grow.

Our emoji animal for this value is the ant, where through the small, coordinated
efforts of many members of the hive, they are able to achieve outsized results.
🐜

- [GitLab Values (Iteration)](https://about.gitlab.com/handbook/values/#iteration)

## Transparency

We respect and listen to each other’s and our users' viewpoints, and have “short
toes” for feedback. Clear written communication is the cornerstone of how we
give updates, since this allows for deeper async thought and reflection. We are
also transparent publicly - feedback from our users on our iterations is the
best way to be sure we’re on the right track. One of the ways we live this value
is by sharing videos of us working to our
[YouTube channel](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg).
Check our [team page](/about/) for links to individual streaming channels.

Our emoji animal for this value is the dog, because as pets they are open,
honest, and sincere. 🐕

- [GitLab Values (Transparency)](https://about.gitlab.com/handbook/values/#transparency)
- [Guidance on Feedback | GitLab](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/)

## Curiosity

None of us can do it alone: we won’t be successful without everyone's (including
our users') contributions, so we must be good at observing and listening to each
other. Doing so requires trust and mutual respect, assumption of positive
intent, and it means engaging with each other with sincere curiosity and a
desire to understand and help. By being connected in this way, it helps us have
the important, fun, and even sometimes difficult conversations that are needed
to build something that will really people.

Our emoji animal for this value is the otter, who are known for the curious,
friendly, and playful nature. 🦦

## Resilience

Building a startup is a journey of learning and trying things, not all of which
will be successful - it is an environment with a lot of uncertainty and change,
and is more a marathon than a sprint. We embrace this uncertainty by being “not
merely determined, but flexible as well”, and excelling at “adapting [our] plans
on the fly.” [1].

Our emoji animal for this value is the badger, since they are known for being
tough and determined, and not backing down from a challenge. 🦡

- [Relentlessly Resourceful](http://www.paulgraham.com/relres.html)
