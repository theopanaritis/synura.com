---
title: Product Development
description:
  We are a product-led company, and how we design and build our product is
  critical to our success. This page outlines the principles and practices that
  guide us.
---

# {{% param "title" %}}

{{% param "description" %}}

## Iteration

In line with our [iteration value](../values/#iteration) we build features in an
iterative way in order to reduce the risk of going down a path where our
customers don't actually get meaningful value from what we build.

This [iteration office hours](https://www.youtube.com/watch?v=zwoFDSb__yM)
recording from GitLab highlights some of the ways you can think about decreasing
iteration size.

### Minimum viability

What it means for features, products, and changes to be minimally viable is
important to understand. It's important to think of it not as delivering a broad
base of capabiliites at a minimal level, but rather as building up a single use
case in a meaningful, differentiated way; nobody is inspired to adopt a creative
tool that does the basics and nothing more. It should be inspiring; it should
delight our users. It should include elements of
[emotional design](https://en.wikipedia.org/wiki/Emotional_Design).

![MVP Pyramid](/images/handbook/mvp_pyramid.png)

Similarly, it's important that each iteration is itself valuable. First build a
skateboard, then a scooter, then a bike, and so on. Make sure that the users are
getting value at each step, otherwise we aren't realy learning anyhing until, to
use the example from the image, the car is already complete.

![MVP Steps](/images/handbook/mvp_usefulness.png)

### Minimize merge request size

Just as with product design and requirements, iteration is important for how we
code as well. The GitLab handbook contains a fantastic guide on
[how to keep code iterations small](https://about.gitlab.com/handbook/engineering/workflow/iteration/#overview),
including when to vertically or horizontally slice.
