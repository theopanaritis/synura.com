---
title: Direction
description:
  Our company direction (mission, use cases, and next steps) is something we'll
  iterate on constantly, but always keep a north star.
---

# {{% param "title" %}}

{{% param "description" %}}

You may wonder why our direction is public. In line with our
[transparency value](../values/#transparency), being open and receiving constant
feedback is the best way to ensure we’re on the right track. If you see
something here that you really love, or something where you think we're missing
the mark, please [let us know](mailto:contact@synura.com).

You can see a recent walkthrough of this page here:

{{< rawhtml >}}

<iframe class="uk-margin-bottom" width="560" height="315" src="https://www.youtube.com/embed/FcCkieIhvpI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{{< /rawhtml >}}

## Mission

The potential impact of video for brands and creators is obvious, but there is
still much to be done to make it easier to create, especially when working as a
team. Our mission is to make it so anyone can easily collaborate on great
looking, brand-aligned, and impactful streaming or on-demand videos. By enabling
anyone to take part in the creative process, we will help everyone reach their
audiences with content that resonates more, giving them a big advantage in a
world where video is the dominant medium.

We envision ourselves eventually providing tooling that helps across the entire
production process: pre-production (storyboarding, planning, strategizing,
script writing), post (transcription, re-framing, other post-processing), as
well as distribution and publishing.

## Use cases

We have a few use cases that we are focused on first:

### Team-based collaboration

There's a lot that goes into producing high quality video content that isn't
obvious at first; script writing, asset management, and ideation all involve
many participants and require coordination of all kinds of artifacts. Once the
video is recorded, captioning, post-processing, and other reviews and approvals
still need to happen. This is complex enough that working in an ad-hoc manner
quickly falls apart, and simple tools that facilitate this process but otherwise
stay our of your way are needed.

### Cloud portability

Being able to store your recordings, scenes, configuration, assets, and
everything else you need and access it from anywhere will ensure you're always
ready for action, and that everyone you're collaborating with is always working
with the latest versions. Having cloud backups is also important for when the
unexpected happens.

### Simplified publishing and engagement workflows

Publishing is only getting more complex, with multiple platforms to deliver
content to, some public and some private. Each of them has its own patterns of
audience engagement, from chat to comments and everything in between, and your
team may have different review and approval workflows for each. They all come
with their own file requirements and best-practices to drive user egagement.
Simplifying this into something easier to understand will help avoid mistakes.

## Guiding principles and assumptions

Our assumptions will evolve and change as we learn more about our market,
customer, and business.

### Video is a key communication medium

The world has become video-first. Many people's favorite celebrities are now on
YouTube and Twitch; some of them have even tried streaming or producing
on-demand content of their own. They expect chat and other forms of
interactivity associated with video content. The bar for quality is very high.
Companies and creators that understand this and execute well will have an
advantage, and platforms that help them achieve this will be indespensible.

### Producing video is challenging

Creating high quality videos is hard. Many people lack the experience or
education to do so, and many of the tasks are repetitive (for example,
transcription and re-framing/editing for mobile or different social networks).
Lighting, audio, framing, script writing are technical skills, and many teams
and creators are learning as they go.

We can help them be effective by building smart defaults and helpful templates,
by offering high quality post-processing that takes away the drudgery, and by
providing learning resources (such as blog posts and videos) that guide them to
creating more effective content.

Transcription/captioning deserve a special callout here. Viewership of videos
without captions is half of what it is for videos with them, but there are no
great existing solutions for transcription that make it easy and produce high
quality, reliable results. Solving this could be incredibly valuable, but it
must actually be better than what's available elsewhere.

### Open source is the best foundation

Although we work with any video recording app, Synura is built with
[Open Broadcaster Software®️ (OBS)](https://obsproject.com/) in mind from the
start as our premier integration. There is no better video recording and
streaming software in the world: it is
[battle-tested](https://www.wired.com/story/anyones-celebrity-streamer-open-source-app/)
against all kinds of real-world use, and is free and open source. The OBS core
application has been
[contributed to by nearly 500 people](https://github.com/obsproject/obs-studio/graphs/contributors),
and we also make a
[monthly $10k contribution](https://opencollective.com/synura) to the project.

Rather than reinventing the wheel, we want to provide add-on production and
cloud syncing capabilities that connect with OBS and make it better for cases
where you're collaborating with a team.

## Customer profiles

There are a few different kinds of users that we can help.

### Companies and brands

There are companies that are already working extensively with video: hosting
webinars, publishing to YouTube channels (typically demo style content),
creating advertisements and/or other pre-recorded sales and training materials.
They understand the challenges and value of video and would benefit from better
collaboration. These kinds of users tend to have the most ad-hoc processes for
managing video and can benefit the most from a collaboration tool, and as such
are good first users to acquire.

### Agencies

There are two kinds of agencies that we can help:

- Brand and advertising agencies produce video content for their clients, which
  are typically companies that don't want to grow video expertise in house.
  Their workflow is similar to if it was the company itself doing it, but has
  the additional requirement of client reviews (both in pre and
  post-production).
- Creator/influencer-focused agencies ([Jellysmack](https://jellysmack.com/),
  [Underscore Talent](https://www.underscoretalent.com/), and
  [ScreenPlay](http://www.screen-play.org/) for example) provide various add-on
  services to creators, such as remarketing (and reworking content) for
  different social networks, operating channels, optimizing strategy, and more -
  even including things like financial management, booking, and so on. These
  agencies have more collaboration in pre-production, while post-production
  (modification and distribution of content to other networks) is primarily a
  hands-off factory type model for the creators.

Agencies tend to be more mature in their video production process and
expectations around collaboration tools, and as such may be better to work with
once we've built a solid foundation with the enterprise.

### Independent creators

In situations where a small independent team produces content (good examples are
educational channels which have a small team that produces a video monthly),
they would likely benefit from the very same collaboration features.

They are sort of a middle ground between the agency and company use cases, in
that they have some unique social media requirements and have more professional
production processes, but they also tend to run on smaller budgets than either
agencies or companies. We should be welcoming to this kind of user by having a
pricing tier that supports them.

## Where we are now

We have not yet launched the product publicly and are working with early
adopters to determine their most important problems to solve and what their
priorities are. This includes talking to their support, sales, and marketing
teams, as well as independent creators. If you are interested in sharing your
feedback, please sign up for our waiting list on our <a href="/">home page</a>.

Our most important immediate task is to
[build an organization that can learn, discover and iterate](../product_development/).

### Initial iteration

We want our [first iteration](../product_development/#minimum-viability) to
immediately adds value, prove out the product concept, and serve as a foundation
from which we can build more advanced features. As with everything on this page
this concept is likely to evolve, but for now it is likely that we will build an
MVP user flow as follows:

1. Come to Synura website and register an account.
2. Download OBS with the Synura plugin installed and pre-configured for account
   username, in order to make it as easy as possible to get going. Instructions
   for setting it up yourself are also available in case you are already using
   OBS.
3. The plugin provides cloud syncing of settings, and automatic backup of
   recorded videos to Cloud.
4. The web interface allows for managing metadata on a video (tags, description)
   and browsing/deleting/downloading your saved videos.

This will be a single-user experience at first. The second iteration will likely
allow for inviting others and having comment threads on saved videos, bringing
the initial collaboration workflows into the product.

### What we aren't doing

- There are other products out there that aim more at the professional video
  production market, with clients like Vice, Netflix, and others. We are not
  focusing on this use case for now because we believe there is an underserved
  segment in the enterprise and prosumer segments that we can better and
  uniquely address, and technology like C2C (connectivity on expensive
  filmmaking equipment) is not as relevant for our segment.

## Competitive landscape

We expect to compete most directly in the
[Video CMS](https://www.g2.com/categories/video-cms) category, with a unique
focus on open-source broadcasting technology and with streaming as a first-class
concept.

We also expect to build a platform that takes the best innovation from the
adjacent categories below, not limiting ourselves to the rigid definition of the
Video CMS category. Our
[Competition / adjacency matrix](https://docs.google.com/spreadsheets/d/1adXUZBm-a3FdQKGG68Y26tU-X7M8bL7dTCtaAbz7YXI/edit#gid=0)
(internal only) has a detailed analysis of the adjacencies.

### Enterprise video CMS

[Video CMS tools](https://www.g2.com/categories/video-cms) allow businesses to
organize, share, modify, and distribute videos internally and/or externally.
[Frame.io](https://frame.io/) is an innovative leader in this space, but was
recently acquired by Adobe and integrates best with their tools; this
[article](https://www.techtarget.com/searchcontentmanagement/news/252505638/Adobe-buys-Frameio-cloud-video-review-platform-for-1275B)
contains a strategic overview of why Adobe bought Frame.io. They are very
upmarket, with competitive advantages in C2C (cloud connectivity for expensive
cameras) and other features for professional film crews. As such, we can
differentiate a compelling product by focusing on regular creators with smaller
budgets, who are supportive of Open Source, and who still need collaboration
tools.

Other alternatives to Frame.io include:

- [Wipster](https://www.wipster.io/) supports all kinds of documents, including
  video, and probably the closest direct competitor
- [Vimeo Review](https://vimeo.com/features/video-collaboration)
- [Dropbox Replay](https://www.dropbox.com/replay)
- [Assemble](https://www.assemble.tv/)
- [Krock](https://krock.io/) focuses on video and animation)
- [Trackfront](https://trackfront.com/) has collaboration plus budgets quotes,
  etc.
- [Kollaborate](https://www.kollaborate.tv/)
- [Kitsu](https://www.cg-wire.com/en/kitsu.html) has progress tracking as the
  basic idiom.
- [Filestage](https://filestage.io/)

None are open source/open core.

### Digital asset management

[Digital Asset Management](https://www.g2.com/categories/digital-asset-management)
(or DAM, sometimes also referred to as Media Asset Management or MAM) is a
category focused on working with all kinds of digital media, sometimes including
video. In cases where they support video, there is some natural overlap with
video CMS. Typically, these tools are used by brand and marketing teams to
ensure consistent brand identity, provide indexes of current assets, version
control, and similar workflows.

### Enterprise video meetings

The primary way how enterprises use video today is for remote meetings. Tools
like Loom, Zoom, and Teams lead the space here, with Loom providing perhaps the
most sophisticated product. All of them are focused on recording a live meeting
and sharing the recording, but most have limited capabilities around search and
discovery of recordings, and especially upcoming meetings - this is usually done
via the calendar app, but that does not provide good discoverability for people
who weren't invited. Scheduled and ad-hoc streams are not really yet a concept.

We do not expect to build a video meeting app, but integrations here may be
important, and there are some features that may overlap that we can learn from.

### Game streaming

There are several streaming companies focused only on gaming, who provide not
just video management capabilities but audience management as well (for example,
merch stores, chat plugins, etc.) Some examples of companies in this space are:

- [Lightstream](https://golightstream.com/) (and
  [API.stream](http://api.stream/) sub-product) has raised 11.4M in this space.
- [Streamlabs](https://streamlabs.com/) is owned by Logitech. They had some
  recent
  [controversy](https://dotesports.com/news/pokimane-hasan-obs-elgato-among-those-who-call-out-streamlabs-for-apparent-copy-of-lightstreams-product).
- [Twitch Studio](https://www.twitch.tv/broadcast/studio) was created as an
  easier-to-use alternative to OBS for streamers.

Although gaming isn't our focus, there is certainly much to learn by looking at
how creators interact with these tools, and translating that to business. In
many ways gaming is way ahead of the game when it comes to how they connect
creators/brands to their audiences through video.

### Stream and video recording/editing

On the recording side,
[Open Broadcaster Software®️ (OBS)](https://obsproject.com) is the clear open
source winner here. Adobe offers the free
[Premiere Rush](https://www.adobe.com/products/premiere-rush.html) app, and on
the Mac there is also [iMovie](https://www.apple.com/imovie/).

Preparing recorded video for publishing is an important step in the process.
There are open source video editors like
[Olive](https://www.olivevideoeditor.org/) and
[Kdenlive](https://kdenlive.org/en/). In the proprietary space, the standard
professional editing app is
[Adobe Premiere](https://www.adobe.com/products/premiere.html), but there are
also companies like [Descript](https://www.descript.com/) doing innovative
things - in this case, Descript uses transcription to identify moments in the
video and allows for editing/removing "umms" by editing the transcription text.

Some products focus on editing automation. [Kamua](https://kamua.com/) does
automated AI-powered video editing; they were bought by Jellysmack, and did a
lot of the most painful manual editing flows.
[Wondershare](https://www.wondershare.com/) is similar, and also provides
effects.

### Professional video production

Video professionals working in film or television also require similar editing,
approval, and workflows that business/marketing workflows require, except with
much larger teams and more complicated production methods. Similar to gaming,
there's probably something to learn here around how to improve our product by
analyzing how they work.
[DaVinci Resolve](https://www.blackmagicdesign.com/nl/products/davinciresolve/)
is one such collaborative cloud software for making films.

### More minimalist offerings

[Apps4rent](https://www.apps4rent.com/obs-open-broadcaster-software-streaming-hosting/)
offers hosted OBS instances in the cloud that you control locally but handle
file backups and processing on their end. It’s a very simple, no-frills offering
but one that already provides value for some users. They describe their
advantage over solutions like Lightstream as "Lightstream doesn't use much of
CPU space but fails to provide as many features as OBS. If you want to enjoy the
tons of features offered by OBS studio without compromising with the lightness
and portability that Lightstream offers, you can do so by subscribing to
dedicated OBS hosting plans offered by Apps4Rent."
