---
title: Marketing & brand
description:
  This page includes information about our brand and marketing guidelines,
  logos, other brand assets, and resources for teams working on campaigns.
---

# {{% param "title" %}}

{{% param "description" %}}

## Name

The name of our company is 'Synura' with the S capitalized.

## Color

Our brand color is [#ff4e2d](https://www.color-hex.com/color/ff4e2d). In other
formats this is `RGB: 255, 79, 46`, `CMYK: 0, 84, 86, 0`, and `PMS: 171 C`.

## Logos

Our logos in various formats (icon, stacked, side-by-side) can be found in
[in the brand folder of our site repo](https://gitlab.com/synura/synura.com/-/tree/main/static/images/brand/).

The name of the company, when displayed next to the logo, is as it appears at
the top of this site. The font is Helvetica Neue, weight is 500, and letter
spacing is 0.2rem.

## Marketing assets

Company assets, including full resolution versions of the logo, are TBD and will
be on our Google Drive once available.

## Company letterhead

Our company Letterhead is TBD will be on our Google Drive once available. This
is read-only; make a copy to edit.

## We are not Open Broadcasting Software®️ (OBS)

[Open Broadcasting Software®️ (OBS)](https://obsproject.com) is a
cross-platform, open source tool for recording and streaming videos. It includes
features like compositing, integration with Twitch chat, encoding/exporting
videos, and more. We provide cloud-based services that make using OBS in a
portable way and with teams easier, but we _are not_ OBS, we do not wrap or
replace OBS, and we should not refer to ourselves as doing so.

When we refer to this software, we should include the ®️ as done above.

## Vision

See our [direction](../direction/) for our vision. It's important we lead with
our guiding principles and assumptions, especially where it touches on use cases
and personas of our likely buyers.

## Marketing audiences

Our audience and personas are TBD. For each, we will provide a description, the
style of marketing they prefer, some examples of content that appeals to the
audience, what other vendors in the space are doing, and our goal for the
audience (for example, qualified leads).

In the meantime, the [direction page](../direction/#competitive-landscape)
contains a competitive landscape.

## Kinds of marketing

### Outbound

- Account-Based Marketing
- [Social Media](#accounts)
- Targeted ads
- Note that to manage LinkedIn company page, your personal LI account must be
  added as an admin. There's no separate login via 1Password

### Inbound

- Website
- Blog Posts
- Overview Deck
- Traditional Earned Media (eg news articles)

## Accounts

Synura has the following social media accounts:

- [Twitter](https://twitter.com/SynuraHQ)
- [YouTube](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg)
- [LinkedIn](https://www.linkedin.com/company/synura)

They are not official channels, but some employees also have streaming channels
where they work on Synura. Links to these can be found on our
[team page](/about/).

## Twitter

### Purpose of building a Twitter presence

1. Build a network of people interested in enterprise video.
2. Increase visibility and credibility in the tech scene.
3. Leverage already existing connections and affiliations (by following and
   getting followed by industry leaders).

### General Twitter guidelines

1. Tweet from our account
2. Keep the tweet concise.
3. Limit hashtags to 1-2 per Tweet.
4. Include a clear call-to-action where applicable (i.e.” Read the full story
   here”).
5. Have a conversational, informative and optimistic tone. Avoid snarky or
   cynical tone, and don't cut down others. That's not who we are.
6. Monitor events and trending conversations to tweet appropriately.
7. Use images, GIFs, and/or videos whenever possible.
8. Use emojis to add emotion when it applies.
9. Ask questions and run polls (see poll ideas here
   [https://business.twitter.com/en/blog/engaging-twitter-poll-ideas.html](https://business.twitter.com/en/blog/engaging-twitter-poll-ideas.html)).
10. Do not express political views.

### Tweet approval process

For now, there is no special tweet approval process but if you want someone to
review it just grab another team member.

### At which frequency should we tweet?

1 to 2 times a week, in addition to our bi-weekly sprint updates

### Who should we follow?

1. C-level and executive from successful and interesting startups, potential
   partners, or publishing platforms (i.e., Twitch, YouTube)
2. Companies and people that produce interesting, innovative video and streaming
   content.

### What should we tweet?

- Product updates and app features.
- Media content from events taking place at (i.e. NAB, etc).
- Original blog posts (note: format links with Bit.ly).
- Blog posts and news articles written by other platforms about making great
  video content.

## Sprint updates

At the end of each sprint, CEO posts an update to a variety of social media
channels. Here is the process:

1. Videos from each team are complete by AM on the Wednesday of the sprint end.
   See demo process for details on this.
2. Confirm that all videos are compiled in a YouTube playlist titled Progress
   Updates YYYY-MM-DD
3. Add CEO video as first video in playlist; reorder the others roughly with the
   most important / informative updates nearest the top
4. Post the video and 1-2 highlights to Twitter from our account.
5. CEO retweets from his personal account and schedules the same post in our
   other media channels, including LinkedIn.

### Sprint update video guidelines

1. Deadlines:
   1. All individual-contributor (IC) videos are due by **end of day Tuesday**
      local time, unless your manager has set an earlier deadline
   2. All manager videos are due by **Wednesday 12p (noon) Pacific time**
   3. CEO video is due by 9:30p Pacific Time
2. All IC videos should be 2 minutes or less, and cover what work you completed
   in this sprint. Manager videos should be 5 minutes or less. (Times are not
   strictly enforced, e.g. going 2m10s is fine, but 3 minutes should be a sign
   that you're covering too much.)
3. Each video should have the person's name, or role if preferred, and the date
   in its title. Remember that dates should be in yyyy-mm-dd format
4. All team members should add their videos to the week's playlist. The first
   person to publish the video may need to create the playlist, titled Progress
   Updates YYYY-MM-DD
5. Videos should not include any user PII or customer names for which we don't
   have explicit permission. If inadvertently shared or shown in a screenshot,
   either re-record the video or mark it as private.
6. For engineers: front-end features should be shown on production and in mobile
   view. Back-end features should show code snippets or handbook documentation.
7. For non-engineers: accomplishments or learnings should be shared rather than
   that an event took place. e.g. "We trained 5 users and they were able to
   complete xyz (or had difficulty with abc)" is much better than "we completed
   a training"
8. Heads of Departments will include a one-slide update on OKRs on each sprint
   update
