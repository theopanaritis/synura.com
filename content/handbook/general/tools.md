---
title: Official company tools
description:
  Here’s a list of tools that the company licenses for shared productivity or
  for which we have company accounts.
---

# {{% param "title" %}}

{{% param "description" %}}

- [1Password](https://1password.com/) (company license): Store shared passwords
  and account information
- [Asana](https://asana.com/) (OCV license): Workflow tracking
- [Bamboo HR](https://bamboohr.com/) (OCV license): Hiring, onboarding,
  recruiting, terminations, training
- [Bill.com](https://bill.com): Manage invoices and accounts payable
- [Brex](https://www.brex.com/) (OCV license): Company credit card
- [Buttondown.email](https://buttondown.email/) (free tier): Landing page
  signups
- [Calendly](https://www.calendly.com) (company license): Calendar scheduling
- [Earth Class Mail](https://www.earthclassmail.com/) (OCV license): Manage
  postal mail online
- [GitLab](https://www.gitlab.com) (free tier): Source control and website
  hosting
- [Google Analytics](https://analytics.google.com/) (free): Website
  instrumentation
- [Google Workspaces](https://workspace.google.com) (company license): Email and
  office applications
- [Greenhouse](https://www.greenhouse.io/) (OCV license): Applicant tracking
  system and job board
- Gusto: US Payroll (not set up yet as we have not hired our first US employee)
- [LinkedIn](https://linkedin.com/) (free): Social media
- [Mention.com](https://mention.com/en/): Brand monitoring (see #mentions
  channel in Slack)
- [Pexels](https://www.pexels.com/) (free): Free stock video footage
- [Pave](https://www.pave.com/): Salary benchmarking for offers
- [Pilot.co](https://pilot.co/) (OCV license): Payroll
- [Silicon Valley Bank](https://www.svb.com) (OCV license): Manage cash and
  make/receive wire transfers
- [Slack](https://www.slack.com) (company license): Team communication & chats
- [Twitch](https://twitch.com) (free): Live streaming
- [Twitter](https://twitter.com/) (free): Social media
- [YouTube](https://www.youtube.com/) (free): Social media

These tools may be used, but are still under review or are being set up:

- Coursera: Learning & development
- [CultureAmp](https://www.cultureamp.com/): Performance Review Tool
- [Expensify](https://www.expensify.com/): Submit expenses to finance
- LinkedIn Learning

Communication is a key linker across all these tools, so check out our
[communication page](../communication/) for details on how we use them.
