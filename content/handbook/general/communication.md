---
title: Communication
description:
  This page describes our communication processes and norms for things like the
  handbook itself, Slack, Google Docs, and any other communication medium.
---

# {{% param "title" %}}

{{% param "description" %}}

## Communication norms

Remote communication can be difficult at times, especially spanning timezones,
cultures, work-styles, a variety of applications, and sometimes challenging
connectivity issues. The following best practices are adapted from GitLab’s
[communication guidelines](https://about.gitlab.com/handbook/communication/) and
meant to help us maximize our asynchronous productivity. Some are general and
some application-specific. In general:

1. Always acknowledge receipt of messages sent directly to you or in which you
   are mentioned. In email, send a “thank you” or “okay” or “got it;” in
   calendar respond accept or reject (or even maybe) to invitations; in Slack or
   GitLab a thumbs up. Acknowledging lets the sender know the message has been
   received and reduces the risk of “oh I was unaware.” It also alerts the
   sender that if they don’t receive a response it might have been overlooked
   and they should check in with you.
2. Favor written over verbal communication for decisions. If you work through
   something verbally, write it down so it can be shared with others. See also
   the section on [writing things down](#writing-things-down) on this page.
3. Use roles instead of names when writing in the handbook. This is primarily
   because good processes are described by the actions required and the role of
   the person playing that action. Individuals play certain roles on certain
   projects, but may shift and change quickly even while the process stays
   static.
4. Timezone should always be specified. We operate in many time zones (e.g. PT,
   CT, WAT, CAT, EAT, HKT, IST), so this is important to avoid ambiguity.
5. Before sending a Google Calendar invite, please ensure that a link to a
   document is included instead of a file attachment. Adding an attachment
   doesn't allow users on iOS Calendar to click into the document. Links work on
   both Google Calendar and iOS.
6. Use numbered lists instead of bullets. They allow for being able to reference
   easily in conversation in cases where it wouldn't be otherwise obvious.

### Writing things down

> "If you’re thinking without writing, you only think you’re thinking." — Leslie
> Lamport

One of the absolute most important things about working async successfully is
having a culture of writing things down. Our handbook is part of this; it's
important that we always keep it up to date, and whenever we answer a question,
we answer it with a link to the handbook. This extends into any kind of
collaboration document (Google Docs, issues, etc.) and ensures that any
information someone might need is not dependent on anyone being available in
real-time.

Furthermore, by writing things down you can reveal gaps in thinking. Often
during a conversation, people can hear the same things and come away with very
different impressions. Writing things down helps avoid this by nature of being
more formal. It also provides a place for edge cases that might otherwise have
been glossed over to be enumerated.

Finally, writing things down ensures that people have time to reflect on how the
react to or engage with the document. In a live meeting, there can be a lot of
pressure to feel like you need to come up with a response right away. Writing
things down allows you to "sleep on it" and come back with a better response.

## Time zones

We aim that most of our work should be "asynchronous" -- meaning that you can do
it on your own schedule and not be dependent on meeting with another teammate in
order to tackle it. However, coordination is also important both for work and
social reasons. Since we span time zones, we ask that team mates make themselves
generally available on Slack and for meetings during those hours that are the
best overlap between those they work with.

## Date format

Dates should be written in [ISO format](https://en.wikipedia.org/wiki/ISO_8601):
yyyy-mm-dd. This is unambiguous, machine readable, and sortable. This is
especially important because we work in multiple countries; some default to M/D
and some to D/M, so "5/4" could be interpreted as "April 5" or "May 4."

While it's recommended to specify yyyy-mm-dd for important communications, it is
acceptable to use mm-dd as a shorthand. It is not acceptable to use slash ("/")
or m-d. For example, it is okay to use "05-04" for "May 4", but not okay to use
"5-4", "4-5", "5/4", or "4/5". Better still is to use the year, or the ISO
recommendation of "--". For example: "2021-05-04" or "--05-04".

## Sharing bad news

Share “bad news” quickly and straightforwardly. Be thankful to receive it -- bad
news is so much harder to share than good news. We’re working on really
ambitious and challenging projects; things will go wrong, and sharing them
quickly and openly can usually provide good fodder for improvement for the whole
company if handled with respect.

One thing that can make bad news so difficult to share is that it may come with
a sense of self-recrimination. In this case it can be helpful to acknowledge the
feeling first (e.g. "I'm concerned that I might have made a mistake") and then
to forge ahead and share the news. We all make mistakes; it's easiest to help
recover and learn from them if they're acknowledged straightforwardly.

Bad news also may be difficult to share if it comes with a sense of blame, e.g.
if you believe someone else made a mistake. In this case, you should consider
the mistake a separate issue from the bad news and consider sharing it as
"negative feedback."
[Give negative feedback in the smallest setting possible](https://about.gitlab.com/handbook/values/#negative-feedback-is-1-1)
-- e.g. a 1:1 video call. Treating the bad news and negative feedback as
distinct issues allows you to consider the timing and format of sharing each
independently.

## Handbook

Always try to answer questions about how we work with a link to our handbook; if
you are unable to find the answer, or if the information is out of date, please
update the handbook first and then send the link. This ensures that our handbook
always remains our source of truth.

### Style

1. Use sentence case for headings, so `This is a heading` and not
   `This Is A Heading`.
2. Removing and re-summarizing old or long-winded content is as important as
   adding new information. This helps us keep our handbook easy to use,
   approachable, and useful for everyone.

### Readmes

The handbook contains a readme folder where you can add a README that people can
use to understand how to work with you. Think of it as a place for anyone who
might work with you to go read a bit and get to know you better; this can be
helpful with remote companies since some of the more casual ways of getting to
know each other aren't possible. You can see
[Jason's README](../../readmes/jason.yavorska/) as an example.

## Slack

At Synura, we do not send internal emails to each other. Instead, we prefer to
use Slack to communicate. We use threads in Slack as much as possible, since
they help limit noise for other people following the channel and reduce
notification overload. We also configure our working hours in Slack to ensure
everyone knows when they can get in touch with each other.

### Slack channels

We have specific channels for various topics, but we also have more general
channels for the teams at Synura.

- _#general_: announcements for everyone to see
- _#thanks_: sharing notes of appreciation with each other
- _#marketintel_: interesting competitive details we've heard of

### Slack conventions

Conventions for using Slack:

1. Use public channels whenever possible. Threads within public channels can
   prevent spamming people, while still allowing others to be aware of what’s
   going on and to contribute if they have something to add.
2. Posts will be considered FYI unless they mention a specific person @mention
   individuals to flag specifically for them
3. Emoji acknowledgements are appreciated. See more on
   [GitLab’s page](https://about.gitlab.com/company/culture/all-remote/informal-communication/#using-emojis-to-convey-emotion).
4. When communicating time in slack, prefer to specify in team's respective
   timezone or use UTC.
5. Use @Channel only for important announcements.
6. People can "browse all channels" and choose which channel to join, all
   internal Synura channels are open to employees. If you would like to rename
   or change the name of a channel please contact your Slack Administrator
7. Slack channels, even private ones, should never be used to discuss specific
   personnel issues, concerns, or performance. Membership in Slack channels
   changes over time and new members also see history; messages posted in
   channels therefore may at some point be visible to more people than who are
   included in the channel when it is written. (Personnel issues should properly
   be addressed in Slack DMs, email, Google Docs that are labeled with
   [restricted] and shared only with specific people, or in our Applicant
   Tracking System for candidates.)
8. When responding in a public channel start a thread using the dialog above and
   to the right of the initial message. This will keep the conversation
   available for all to see but make fewer “unread” messages for teammates that
   are logging in after not checking Slack for a few days. Look for this icon:
9. We can create "external" channels to improve communication with clients or
   external collaborators. The channel will be limited to collaborators with an
   invitation sent by email by our Slack administrator but open for all Synura
   employees once the group is created. A list of participants should be
   provided and the main purpose of the channel should be specified.
10. Note that Synura has a goal to have 80% of communication in public channels.

## Google Drive

Generally speaking, when you share a document share it with [insert group
email]. We want to allow anyone to contribute to all company work products, and
this ensures that when someone needs to access it they can. (There may be some
specific cases where information or access rights need to be more restrictive,
but please do it sparingly.)

Documents should use these prefixes:

- _[public]_ - open to everyone, even outside of Synura
- _[synura]_ - available to all at Synura (assumed if not labeled)
- _[restricted]_ - shared, but only to specific individuals. May also want to
  adjust the sharing settings
- _[private]_ - might be useful to put this for any files that you wish to keep
  to yourself.

To use documents that are stored in a shared drive, it is helpful to create a
link to that shared drive from your personal drive. To do this:

1. Head to [drive.google.com](https://drive.google.com)
2. Click on “Shared with me”
3. Tap the name of the folder you want to add to your Google folder
4. Click on the name of the folder
5. Click on “Add shortcut to drive”

### Visible to all by default

By default documents are viewable by everyone at the company. This means anyone
with a Synura email address can see the document if they have the link, but it
will not show up in their search results.

When a document should have [limited access](#limited-access), adjust the
document to Restricted. Do this by selecting Share --> Change under "Synura
Anyone in this group can view" --> Restricted from the dropdown.

### When sharing, give edit access to all

If you do share the document with anyone, share it with everyone@synura.com, but
communicate directly with any individuals from whom you need input. This allows
everyone to contribute and saves a lot of time when one stakeholder decides to
add another, who may then need to request access from someone who has already
stopped work for the day.

When sharing, uncheck the Notify people checkbox. It's not recommended to use
Google Drive to notify, since you will share with everyone@synura.com. Share
with everyone, but notify only those who you want to act upon it.

Conversely, documents shared to everyone@synura.com should be considered “FYI”
unless someone follows up directly with you. There’s no obligation to read every
document that’s shared.

### Be cognizant of PII

If you do share a document that includes Personally Identifying Information
(PII) from one our partners share it with the specific group (Ex:
data-partner@synura.com) instead of everyone@synura.com, this will follow our
Not Public and Limited Access policy.

## YouTube and Twitch

We post our sprint meetings on
[YouTube](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg), and also
are encouraged to stream our work on Twitch. We value transparency about what
we’re working on and you’re welcome to share strengths and weaknesses, works in
progress, highlights or concerns. However, we must also respect that not every
external partner will share this level of comfort. Therefore we must be sure to
avoid any of the following, in both our verbal narrative and any screens we
share in presentations:

1. Personally identifying information who are not Synura team members. E.g. no
   mention of names, license plates, phone numbers, emails, etc.
2. Statistics or metrics of our partners’ data.

## Not Public & Limited Access

We make things public by default because transparency is one of our values. Some
things can't be made public and are either internal to the company or have
limited access even within the company. The items that are not public are
detailed in the sections below, together with an explanation of why they aren't
public. If something isn't listed in the sections below, we should make it
available externally.

### Not public

Some things are internal, available internally but not externally. The following
items are considered internal:

1. Financial information, including revenue and costs for the company, is
   confidential.
2. Deals with external parties like contracts, vendors, or partners.
3. Content that would violate confidentiality for a Synura team member, partner,
   or user.
4. Legal discussions are not public due to the purpose of Attorney-Client
   Privilege
5. Partner's information is not public since Partners are not comfortable with
   that, and it would make it easier for competitors to approach our Partners.
   Suppose an issue needs to contain _any_ specific information about a Partner,
   including but not limited to the company name, employee names, the number of
   users. In that case, the issue should be made confidential. Try to avoid
   putting Partner' information in an issue by describing them instead of naming
   them and linking to their Google Drive Folder. When we discuss a Partner by
   name, that is not public unless we're sure the partner is OK with that. When
   we discuss a competitor (for example, in a sales call), this can be public as
   our competitive advantages are public.
6. If public information compromises one or more team members' physical safety,
   it will be made not public because creating a safe, inclusive environment for
   team members is essential to how we work.
7. Compensation Changes: Synura will communicate and train team members on the
   output of iterations (Compensation, Benefits), but team members will not have
   visibility into the inputs and decision-making of compensation changes.
8. Security vulnerabilities that we're aware of should be kept to Synura-only.
   More sensitive ones may be further restricted to "Limited Access."

### Limited access

The items below are not shared with all team members. Limited access is a more
severe restriction than internal-only.

1. Deals with external parties like contracts and vendors.
2. Content that would violate confidentiality for a Synura team-member, Partner,
   or user.
3. Partner's lists and other Partner information are not public since many
   Partners are not comfortable with that, and it would make it easier for
   competitors to approach our Partners. If an issue needs to contain _any_
   specific information about a Partner, including but not limited to the
   company name, employee names, and the number of users, the issue should be
   made confidential. Avoid putting Partner information in an issue by
   describing them instead of naming them and by linking to their Google Drive
   folder.
4. Plans for reorganizations. Reorganizations cause disruption, and the plans
   tend to change a lot before being finalized, so being public about them
   prolongs the trouble. We will keep relevant team members informed whenever
   possible.
5. Planned pricing changes. Much like reorganizations, plans around pricing
   changes are subject to shift management time before being finalized. Thus,
   pricing changes are limited access while in development. Team members will be
   consulted before any pricing changes are rolled out.
6. Legal discussions are restricted to the purpose of Attorney-Client Privilege.
7. Some information is kept confidential by the People Group to protect the
   privacy, safety, and security of team members and applicants, including job
   applications, background check reports, reference checks, compensation,
   terminations details, demographic information (age and date of birth, family
   or marital status, national identification such as passport details or tax
   ID, required accommodations), home address. Whistleblower identity is
   likewise confidential. Performance improvement plans, disciplinary actions,
   and individual feedback are restricted as they may contain negative feedback
   between you and your manager.
8. Acquisition offers for us are not public since informing people of an
   acquisition that might not happen can be very disruptive
9. Acquisition offers we give are not public since the organization being
   acquired frequently prefers to have them stay private.

Our default is transparency, but there is sensitive information that can either
not be shared publicly or can only be shared with a limited internal group. We
follow Gitlab's guidelines for
[not public](https://about.gitlab.com/handbook/communication/#not-public) and
[limited access](https://about.gitlab.com/handbook/communication/#limited-access)
information.
