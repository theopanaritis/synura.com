---
title: Recruiting
description:
  We aim to build a world-class team. This page goes through processes for
  internal teams and candidates to help support that goal.
---

# {{% param "title" %}}

{{% param "description" %}}

## Jobs and boards

We use [Greenhouse](https://app3.greenhouse.io/) to host all jobs and manage our
recruiting and interview process. All team members are welcome to participate,
and will be asked to play different roles for different positions. Our open jobs
are listed on [our job board](https://boards.greenhouse.io/synura).

## General factors we're looking for

At a high level, we value teammates with intelligence, integrity, and strong
work ethic. These traits should be looked for during the interview process. In
addition, there are specific factors relevant to all candidates applying to
Synura:

1. **Alignment** - clear what candidate is looking for and that it aligns with
   what we need
2. **Domain experience** - SaaS software, new product launches, startup
   companies. Note: focus on stronger of resume / LI Profile, recognizing that
   different people put emphasis on different mediums.
3. **Tech expertise** - web technologies, agile stack
4. **Leadership** - have a player / coach mentality. Want to contribute by doing
   and by helping others.
5. **Communication** - concise, clear, and able to cite specifics. They write
   and communicate well, and avoid glaring errors. Note that we make affordances
   for challenges commonly faced by non-native speakers. For example, issues
   that spell check can catch should be a red flag, but more subtle grammatical
   or phrasing that may seem awkward to a native speaker should not.

Each job may have other specific requirements that will be kept in the job
description or scorecards within Greenhouse.

## Hiring process steps

We have a series of steps we take everyone through in the hiring process.
Proceeding to the next step is contingent upon success in the previous one.

### 1. Initial introduction call

### 2. Resume deep dive

The hiring manager or a peer leads the Resume deep dive, and sometimes there is
another teammate who shadows the interview. It takes 50-minutes to complete, but
you must be focused.

A resume deep dive (often referred to as a “Topgrade Interview”) is an interview
style where we take a deep dive into a candidate’s background, past to present.
The goal of a resume deep dive is to uncover the motivation behind a candidate’s
decision making, as well as their strengths, weaknesses, and accomplishments in
past roles. Here is a document to help guide expectations on this stage in the
interview process. This is simply an example of what to expect, and does not
necessarily represent the questions we will be asking.

Example questions:

- How did you find your job?
- Why did you choose to accept it? Who hired you?
- What was your day to day like?
- Was this a step up from your previous role? If so, how? If not, why?
- What would your former boss say were your strengths and opportunities?
- What would your peers say about you?
- What did you like most and least about your job?
- What are you most proud of accomplishing while in your job?
- Why did you leave?

### 3. Technical interview (if applicable)

This interview goes into the technical details of the role at hand, so varies
between engineering, design, or other responsibilities.

#### Engineering technical interview process

1. Coding challenge - 2-3 hour exercise to demonstrate your strengths
2. Technical interview - 50 minutes with CTO or appropriate technical resource

#### Designer technical interview process

1. UX team Interview - 40 minutes with a representative of the UX team
2. Competency based interview - 50 minutes with Product Manager or submission of
   the UX case study challenge (document TBD).

### 4. CEO interview

Before your CEO interview, please be sure to read through our
[direction page](../../general/direction/). The CEO interview tends to be
fast-paced and values succinct answers. There's a lot to get through in a short
amount of time, and the best interviews allow for follow up questions on your
initial responses. Here are the some of the questions that will likely be asked;
you're also welcome to ask me similar questions:

1. How would you introduce Synura to a new prospective customer, colleague, or
   friend?
2. Lets discuss our company [direction](../../general/direction/).
   1. What aspects of it excite you most?
   2. Are there areas that you see as potential concerns?
   3. What do you think will be some challenges we will face in the next 6-12
      months and what are your thoughts on how to address them? Please share
      some thoughts about challenges pertinent both to our company as a whole
      and to your role in particular.
3. Tell me about a complex, early-stage project that you worked on. What made it
   challenging? How did you approach it? How does it exemplify how you approach
   your work?
4. What is your philosophy on prioritization, focusing on what’s important, and
   preventing yourself from getting blocked (and unblocking yourself when it
   sometimes happen anyway?)
5. What's your experience with remote and async work? What are some of the
   practices you've developed to be effective? What practices from the company
   have you experienced that were helpful or detrimental?
6. Tell me about a colleague with whom you've worked in the past who you thought
   was truly exceptional. What qualities or practices did they demonstrate? In
   what ways are you similar to them, and in what ways are you different?
7. Please consider the best and next best teams you've worked with. How would
   you characterize them? What did they have in common, and what distinguished
   the best from its next counterpart?
8. What are you looking for in a CEO?
9. What questions do you have for me?

Note that this is not necessarily exhaustive. We may also explore specific
questions that arose from my review of your experience, your fit with Synura, or
follow-ups from previous rounds of interviews.

In the future, we will be creating sprint updates on our
[YouTube channel](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg), and
watching and discussing these will be part of the interview. For now, though, we
will focus on the direction page.

#### Advice on how to interview well with the CEO

Here are some tips on how to have a good interview with the CEO:

- Be concise. Answer directly and specifically; let him ask follow ups as
  necessary.
- Be willing to ask for clarification if needed.
- Be honest and straightforward. Don't try to spin or obfuscate, even if he
  probes in areas you're not proud of.

### 5. References

We request 5 references before making an offer, of which we will speak with at
least 3. This helps us learn more about you from those who have worked with you
closely. In addition to the obvious benefit of helping us assess what you can
bring to the team and gain context on who you are as a person, it also helps set
the stage for a productive working relationship with you because we can ask
advice on how best to support your success once onboard.

We would like to talk with a current or former colleague from each of these
categories. An individual from each of the first three categories is required,
and the fourth if you're interviewing for a supervisory role.

1. Your direct (or formerly direct) supervisor - someone responsible for or who
   oversaw your work
2. A peer in the same role as you - someone who did similar work to you
3. A cross-functional partner or customer - someone who was a consumer of your
   work
4. Someone you've supervised, if you're applying for a management position
5. Someone else of your choosing

We encourage you to share people who have worked with you closely and know your
capabilities well. We also encourage you to ask them to be candid so we can get
to understand who you are and what you can bring to the team. We have a
reference script (document TBD) that we use, but do not share in advance because
we want candid and spontaneous responses.

### 6. Investor interview (when applicable)

Some candidates will be required or interested to meet with our investor. Before
sending interview invitations, it's important to prepare a summary doc with
relevant information (template TBD). The interview doc is internal and should
**not** be shared with the candidate.

**Note for Candidates** We recommend you prepare for your conversation with our
investor. Our investor is [Open Core Ventures](https://opencoreventures.com/).
You can browse the
[GitLab CEO Handbook page](https://about.gitlab.com/handbook/ceo/) and in
particular review the
[pointers from his direct reports](https://about.gitlab.com/handbook/ceo/#pointers-from-ceo-direct-reports)
and context on
[how he conducts interviews](https://about.gitlab.com/handbook/ceo/#interviewing-and-conducting-meetings).

Our advice on how to interview with our own CEO also applies.

**Notes for Synura team** Use the procedure below when scheduling on your
calendar directly or on behalf of someone else:

- Create a separate Google doc for each interviewee
- Add a link to the doc from your calendar invite

Include in the doc:

- Candidate's name and current title
- Candidate's email address
- Link to candidate's LinkedIn profile
- Link to Synura's reference check
- Summary of any particular points to address or known flags to probe on.

### 7. Offer

We use [Pave](https://www.pave.com/) to benchmark compensation generally, but as
per our job listings the offer will depend on a number of factors:

> The actual offer, reflecting the total compensation package and benefits, will
> be at the company’s sole discretion and determined by a myriad of factors,
> including, but not limited to, years of experience, depth of experience, and
> other relevant business considerations. The company also reserves the right to
> amend or modify employee perks and benefits.

Offers should be delivered verbally, then with an email attaching a signed offer
letter (template document TBD), and then with a legal contract. Some team
members join us through an agency, in which case the details are worked out in
the conversation with their agency. For independent, full-time team members, use
this offer template and script (document TBD).

To trigger an offer process, use the following forms:

- [New Employee](https://forms.gle/7sm8BbD5eFPah9CG6)
- [New Contractor](https://forms.gle/S9EXafcXmnhnARMX9)

## Candidate frequently asked questions

Here are frequently asked questions and our answers:

1. What’s is your funding situation?
   1. We’re funded by a single seed investor through a SAFE and not expecting
      revenue immediately. Our goal is to demonstrate product market fit through
      growth and operational customers, and raise in that timeframe.
   2. Our investor is [Open Core Ventures](https://opencoreventures.com/), led
      by Sid Sijbrandij, the founder and CEO of GitLab. He has built a
      successful all-remote company that provides an end-to-end platform in a
      market full of fragmented solutions. In addition to funding, he provides
      advice and relationships to help us learn from and build upon GitLab’s
      success.
2. What’s your stance on remote work?
   1. We’re committed to being an all-remote company with no offices. We think
      this yields better results: it will build a culturally diverse workforce
      necessary to operate in cities around the world, offers great advantages
      to employees like control over personal time and no commute, and is much
      more productive than semi-remote environments that disadvantage remote
      employees over those at headquarters.
   2. This does take an investment in the process to make work, for example:
      good hygiene around documented communications, a handbook that’s
      operationally useful and gets contribution from the whole company, and
      team social chats -- agendaless meetings just for socializing. These are
      good practices anyway, but many companies don’t invest sufficiently
      because so much of their process evolves informally.
3. How many teammates are you?
   1. Our current team is listed on our [website](https://www.synura.com).
4. What do you do and where are you headed?
   1. Check out our [direction page](../../general/direction/) for answers to
      this question.
5. What’s the compensation range?
   1. We aim to pay at the 50th percentile of the local market in which we're
      hiring. We factor in the role, seniority, and location. Your exact
      compensation amount will be based on your experience level and market
      conditions. Some references we use to calibrate on market rates are
      reports from [Pave](https://www.pave.com/),
      [Birches Group](https://birchesgroup.com/),
      [Payscale.com](https://www.payscale.com/),
      [Glass Door](https://www.glassdoor.com/),
      [Salary.com](https://www.salary.com/), and
      [Blind](https://www.teamblind.com/).
6. Have you specified growth targets and timelines? How well are you doing
   against those goals?
   1. Please see details on our [direction page](../../general/direction/)
   2. Please see progress against top-line goals from our latest "Progress
      Update" from our
      [YouTube Playlists](https://www.youtube.com/channel/UCMdZDGdS6Ak4PrSw8l0qeJg).

## Tips for a successful interview

**Arrive on time** For all of our the interviews we try to be respectful of your
time by keeping them as short and focused as possible. Arriving late means we
won't have enough time to complete the interview. Either arrive on time, or if
you know you'll be late reach out ahead of time to reschedule.

**Test your tech in advance** There’s nothing worse than having technical
difficulties during an important call. That’s why it is important to get
familiar with [Zoom](https://zoom.us/) before your interview.

- To begin, download the app to your phone or desktop ahead of time. If you’ve
  never used Zoom before, familiarize yourself with the quick start-up guide for
  new users. See
  [bandwidth requirements](https://support.zoom.us/hc/en-us/articles/201362023-System-requirements-for-Windows-macOS-and-Linux)
- Once you have downloaded the Zoom app, take a moment to start your own private
  meeting to get familiar with the interface and features.
- You can take your test one step further by recording yourself and watching it
  to find areas of improvement.
- Ensure that you have a stable internet connection. Use an ethernet cable if
  necessary. You can check your bandwidth through
  [SpeedTest](https://speedtest.net) or
  [Bandwidth Place](https://www.bandwidthplace.com/)
- Test out your sound. Use headphones to hear clearly and to block outside
  noise.

## Internal referral incentive program for software engineers

Synura is offering a referral bonus to team members who refer a software
engineer that we decide to hire. If you know someone who you think would be a
good fit for a position at our company, feel free to refer them. **If we end up
hiring your referred candidate, you are eligible for $500 referral bonus.**
Additional rules for rewards:

1. There is no cap on the number of referrals an employee can make. All rewards
   will be paid accordingly.
2. If two or more employees refer the same candidate, only the first referrer
   will receive their referral rewards.
3. Referrers are still eligible for rewards even if a candidate is hired at a
   later time or gets hired for another position.
4. Reward will be paid 6 weeks after the candidate starts at the company and is
   in good standing

## Process for recording candidate scores

Each team member should enter their feedback into Greenhouse, without discussing
it first with others. Then we will use 10-15 minutes at the next Strategy call
to discuss the feedback synchronously. In general:

- All team members who participate in the interview process should have
  Greenhouse accounts and be added as Interviewer to the role for which they are
  evaluating candidates.
- All feedback must be added as a Scorecard at the end. Reviewer should note
  observed strengths, any surfaced concerns, and (if appropriate) any open
  questions. Cite specifics in order to help with your memory and help the
  manager and other team members interpret your feedback.
- All Scorecards are summarized with a 4-point rubric, interpreted as follows:
  - Definitely Not - Based on this interaction there were serious cultural or
    behavioral red flags that were surfaced. Scorecard should articulate what
    those were. This could also be used to indicate that there was no evidence
    that the candidate had the necessary skillset; however, hopefully these
    situations are screened out prior to the candidate having an interview.
  - No - Based on this interaction, Reviewer would recommend not proceeding.
    Usually this indicates that the reviewer did not end with a sense of
    confidence that the candidate demonstrated the necessary skill. If the
    candidate did not convince you, this is the option to choose.
  - Yes - Reviewer felt confident that candidate can do the job and will be a
    good addition to the team. Candidate actively demonstrated skill and will to
    perform. Reviewer must cite specifics in order to use this; however, there
    may still be open questions or red flags that are communicated and can be
    followed up on by others.
  - Strong Yes - Reviewer was very impressed and has specific reason to believe
    that this candidate had the necessary skill and will to perform more than
    what's being asked in the role, and seemed exceptional relative to other
    candidates. Reviewer is excited about adding this candidate to the team.
- It is recommended that after each round and all scorecards are complete, all
  reviewers meet synchronously for a few minutes with the manager to provide
  color and context and Q&A about their responses.
