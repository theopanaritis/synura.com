---
title: Onboarding
description:
  This page describes our onboarding procedures, both internal and for the new
  employee, and details on how to get your work computer.
---

# {{% param "title" %}}

{{% param "description" %}}

## Hiring a new team member

1. Reach out to CEO and CTO and give them an opportunity to meet the candidate.
2. Research compensation using [Pave](https://www.pave.com/), making sure to
   adjust for the cost of living where the candidate will do the work. If you're
   unsure of your findings, ask for help from the CEO.
3. Share the compensation information you've gathered with People Ops. They will
   confirm the information with the CEO and document this decision in the
   compensations decision document (document TBD) for future reference.
4. After compensation has been determined, create two documents for the
   candidate:

- "Exit scenarios" spreadsheet
- "Informal offer email" template.

If you need these templates, reach out to People Ops on Slack, and they will
provide you links that automatically create copies. Change the name of the
documents accordingly (e.g., "[candidate's name]'s copy of exit scenarios,") and
link to the exit scenarios spreadsheet from the offer email.

1. Prepare the informal offer email. You'll need to add the following
   information to the template:
   - Candidate's name and email address
   - Candidate's start date
   - Candidate's compensation
   - Candidate's reporting manager
   - Equity offered to the candidate (make this information a link to the
     candidate's exit scenarios spreadsheet)
   - Benefits (determined by the candidate's location)
2. Prepare the exit scenarios spreadsheet. Enter the percentage of equity
   offered to the candidate, and the spreadsheet will update to reflect this.
   Note: Don't play with numbers in the exit scenarios spreadsheet. The revision
   history is visible to the candidate, and they might misunderstand.
3. Once both documents are complete, share the offer email draft, exit scenarios
   copy, and a link to the compensation decision, with People Ops for
   confirmation.
4. After People Ops confirms that everything is correct, CEO or CTO will send
   the offer email. The offer email is copied directly from Google drive and
   sent to the candidate. When they send the offer, CEO or CTO will edit the
   permissions of the exit scenarios sheet and share with the candidate. Note:
   When hiring an international employee, Pilot.co recommends starting the
   hiring process a month before the new employee's start date.

### Steps after an offer is accepted

1. Once an offer is accepted in writing, reply to the candidate, CCing People
   Ops via his Synura email address to introduce the candidate to him.
2. If the new team member is in the United States, People Ops will reach out to
   them and get any information needed (typically the new team member's home
   address and phone number) and send them a consulting or employment agreement
   through [Docusign](https://www.docusign.com/). If the new team member is an
   international employee or contractor, People Ops will start the hiring
   process in [Pilot](https://pilot.co/). Note: International contractor and
   employment agreements are handled by the Pilot.co when you start the hiring
   process.
3. For US employees/contractors After an agreement is signed and stored in the
   correct Google Drive folder, People Ops will start onboarding the new team
   member in [Gusto](https://www.gusto.com/). If the new team member is a W-2
   employee, People Ops will reach out to them and schedule an I-9 verification
   meeting. **Note**: If we're hiring in a new state, we'll have to register for
   state taxes and unemployment. This process can be handled by Gusto. Usually.
4. Before their first day at Synura, People Ops will create a
   [Google Workspace account](https://admin.google.com/ac/users) for the new
   team member, add them to the Synura [insert Gitlab/[insert Gitlab/GitHub]],
   create an onboarding issue in the Synura/confidential [insert Gitlab/GitHub]
   repo, and invite them to join the Synura Slack. If the new team member needs
   to purchase a work computer, People Ops will set them up with a
   [Brex](https://dashboard.brex.com/team/invite-user) card.

### US-based consultants

When retaining the services of a U.S. based consultant, please use the following
consulting agreement (document TBD).

Follow these steps to fill in the Consulting Agreement:

1. Change the '**Effective Date**' located on PAGE 1 under the Title.
2. On PAGE 8
   1. Add the name and title of the Employee executing the Agreement in the
      '**CLIENT:**' section.
   2. Add the name on the consultant in the '**CONSULTANT:**' section.
3. On Exhibit A (PAGE 9) 3. Fill in the '**Project Assignment: #**' Section with
   the appropriate Project NUMBER below the tile. For Engineering, project
   numbers starts with E followed by a NUMBER. 4. Fill in the '**Dated:**'
   Section with the appropriate date below the tile 5. Fill in the date the work
   will start and the date it will end under '**Schedule Of Work:**' 6. Set the
   hourly fee in Section A of '**Fees And Reimbursement:**' 7. Set the maximum
   amount we may pay the consultant for the project in Section C of '**Fees And
   Reimbursement:**' 8. Add the Scope of Work in the '**Project:**' Section of
   the Exhibit A section (PAGE 9)
4. On PAGE 10 9. Add the name and title of the Employee executing the Agreement
   in the '**CLIENT:**' section 10. Add the name on the consultant in the
   '**CONSULTANT:**' section on PAGE 10 11. Fill in the '**Dated:**' Section
   with the appropriate date
5. DO NOT CHANGE EXHIBIT B AND C.
6. Sending the full contract via Docusign or helloSign.
7. Add signature lines for CEO and Contractor on main Agreement (Page 8)
8. Add signature lines for CEO and Contractor on Agreement and Exhibit A
   (Page 10)
9. DO NOT TOUCH Exhibit B and C — these are for reference only
10. Send to CEO and Contractor for signature
11. Email peopleops@synura.com to inform them to set up the consultant with
    a 1099.
12. Save the executed copy of the Agreement to our Shared Drive for contracts.
    (Ask CEO for link.)

### Onboarding a new advisor

Advisor agreements are sent through [DocuSign](https://www.docusign.com/), using
the "Advisor Agreement" template. To send a new advisor agreement, you'll need
the new advisor's name, and the number of shares they are offered. Once the
agreement is sent, add a new row to the advisory board spreadsheet (document
TBD) and enter the new advisor's information. Use this spreadsheet to track the
advisor's progress through the onboarding process.

**Note**: Be sure to mark any columns that haven't been completed yet as "TODO"

When the agreement is completed, make sure it is in the correct Google Drive
folder, and ask the new advisor to add us on
[Linkedin](https://www.linkedin.com/company/synura), Crunchbase (link TBD), and
AngelList (link TBD).

## Workstation setup

### Spending company money

As we continue to expand our own company policies, we use
[GitLab's open expense policy](https://about.gitlab.com/handbook/spending-company-money/)
as a guide for company spending.

In brief, this means that as a Synura team member, you may:

1. Spend company money like it is your own money.
2. Be responsible for what you need to purchase or expense in order to do your
   job effectively.
3. Feel free to make purchases in the interest of the company without asking for
   permission beforehand (when in doubt, do inform your manager prior to
   purchase, or as soon as possible after the purchase).

For more developed thoughts about spending guidelines and limits, please read
[GitLab's open expense policy](https://about.gitlab.com/handbook/spending-company-money/).

### Purchasing a company-issued device

Synura provides laptops for team members to use while working at Synura. As soon
as an offer is accepted, someone will reach out to the new team member to start
this process. They will work with the new team member to get their laptop
purchased and shipped to them.

Team members are free to choose any laptop or operating system that works for
them, as long as the price [is within reason](#spending-company-money).

When selecting your new laptop, we ask that you optimize your configuration to
have a large hard drive (video files are large) and be available for delivery or
pickup quickly, without waiting for customization.

When a device has been purchased, it's added to the
[spreadsheet of company equipment](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0)
where we keep track of devices and equipment purchased by Synura. When the team
member receives their computer, they will complete the entry by adding a
description, model, and serial number to the spreadsheet.

You need to have a good workstation to work from home to be a productive
contributor at Synura. Full-time teammates may use company money to purchase
[office equipment and supplies](https://about.gitlab.com/handbook/spending-company-money/#office-equipment-and-supplies),
as they do at GitLab. (Part time teammates or contractors should ask their
manager before making purchases.) We mostly follow the guidelines described in
their policies, though one important distinction is that within Synura,
purchases exceeding $500 USD are company property and required to be tracked in
the
[Employee Equipment - Fixed Asset Tracking sheet](https://docs.google.com/spreadsheets/d/1d8G-z5UPa25QUsUHiZR2J6EAxulW4oGNVrrlA3YCjiw/edit#gid=0).
There's a link below that provides details on reasonable prices for items on the
list, and look at our [finance page](../../corporate/finance/) for instructions
on how to file expenses.

### Required setup

The following setup is required for all teammates:

- Computer -- see
  [laptop purchase and setup](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptops)
  from GitLab. We recommend
  [Apple MacBook Pro 14", 512 GB](https://www.apple.com/shop/buy-mac/macbook-pro/14-inch-space-gray-8-core-cpu-14-core-gpu-512gb#)
  for non-engineers and
  [Apple MacBook Pro 16"](https://www.apple.com/shop/buy-mac/macbook-pro/16-inch-space-gray-10-core-cpu-32-core-gpu-1tb#)
  for engineers. Team members may refresh their laptop every 36 months. Please
  submit your Apple purchase request to accounting@synura.com.
- [Camera](https://www.logitech.com/en-us/products/webcams/c920s-pro-hd-webcam.960-001257.html?crid=1865)
  for videoconferences
- [Light](https://www.amazon.com/Webcam-Light-Stream-Selfie-Logitech/dp/B07G379ZBH/ref=sr_1_11?dchild=1&keywords=desk+key+light&qid=1609263224&refinements=p_85%3A2470955011&rnid=2470954011&rps=1&sr=8-11)
  that makes your face visible on videoconference calls
- High-speed, reliable internet
- Power source, if it's not reliable in your area. We recommend solar power as
  it is efficient.
- Quality
  [headset](https://www.bestbuy.com/site/logitech-h390-usb-headset-with-noise-canceling-microphone-black/9618932.p?skuId=9618932)
  with microphone for videoconferences **or**
  [external mic](https://www.bluemic.com/en-us/products/snowball/) and
  [headphones](https://www.apple.com/shop/product/MYMD2LL/A/beats-flex-all-day-wireless-earphones-yuzu-yellow?fnode=b4a25f917af76a6a61134216a970e439aaae4b86fdbd20c110280fd0c15ec958071c38fb99ee36b56abb802ffc283f8a0993ec8b242208d7b53ef10758d2fbefb459bcd5ce1a4033548a5add254353db58e62425761af9065cd47c23f4aa4faba4bf87e06d83b261b6c7058a5e1c31e1)
  combo (up to personal preference).

GitLab offers guidelines on
[reasonable prices](https://about.gitlab.com/handbook/finance/expenses/#hardware)
and recommendations on specific pieces of hardware.

### Recommended hardware

These items are highly encouraged:

- [LG Monitor](https://www.lg.com/us/monitors/lg-32UD60-B-4k-uhd-led-monitor)
- Upgraded webcam -- something better than what comes with your laptop, and that
  can be mounted on your monitor
- Comfortable,
  [ergonomic chair](https://www.amazon.com/Hbada-Ergonomic-Office-Chair-Adjustable/dp/B07P7MQ9D5/ref=sr_1_11?dchild=1&keywords=ergonomic+chair&qid=1609265667&refinements=p_85%3A2470955011&rnid=2470954011&rps=1&sr=8-11)
- [Adjustible height desk](https://www.autonomous.ai/standing-desks/smartdesk-2-home?option1=1&option2=1980&option16=38&option17=1881&utm_term=438971237506&utm_campaign=performance&utm_source=google&utm_medium=shopping&utm_content=&hsa_acc=9219256787&hsa_cam=10203432663&hsa_grp=100642041023&hsa_ad=438971237506&hsa_src=u&hsa_tgt=pla-293946777986&hsa_kw=&hsa_mt=&hsa_net=adwords&hsa_ver=3&gclid=CjwKCAiAxKv_BRBdEiwAyd40N11y62j-iUDWXGSOPmqEqUzN9q_PMlgKO_PM-FbPMtaSomNHnQ94mRoChd8QAvD_BwE),
  or a
  [standing desk converter](https://www.amazon.com/VIVO-Adjustable-Converter-Tabletop-DESK-V000V/dp/B0784HWPN6/ref=sr_1_5?dchild=1&keywords=standing+desk+converter&qid=1600277717&sr=8-5)

We recommend you consider these items:

- [Mouse](https://www.apple.com/shop/product/MLA02LL/A/magic-mouse-2-silver) and
  [keyboard](https://www.apple.com/shop/product/MLA22LL/A/magic-keyboard-us-english)
- Coworking space
- Office decorations that help you feel productive or look good on Zoom
- Wifi router
- Android phone
- Internet connectivity
- Google Voice - US-based phone numbers are available to all employees if
  needed, or need to register a phone number with external parties but don’t
  want to share your personal line. Please request Gvoice account from People
  Ops via the #peopleops Slack channel.

Note that internet connectivity may be expensed if it is used primarily for
business and local law permits us to pay for it without incurring taxes. We
recommend you aim for at least 10Mbps, but that you make relevant cost /
bandwidth tradeoffs based on your local area. Expenses should be submitted
monthly using our [expense guidelines](../../corporate/finance/).

### Recommended power backup

To avoid interruptions when there is no electricity, this is a setup we
recommend using:

- 100 Watts Solar panel
- 500VA - 1000va Hybrid inverter
- 100AH 12V Battery

### Shipping hardware from the U.S. to Africa

We recommend using the following parcel forwarding services:

- [Heroshe](https://www.heroshe.com/): to ship goods from the U.S. to Nigeria.
- [iShop Worldwide](https://www.ishop-worldwide.com): to ship goods from U.S. or
  the U.K. to Zambia or Nigeria.

For both platforms, the process is as followed:

1. Sign up to the platform.
2. Order items to the warehouse address indicated on the respective websites.
3. Pay the shipping fees to platform.
4. Receive items in Nigeria or Zambia.

For warranty reasons, we recommend buying directly from the manufacturer rather
than platforms such as Amazon.

## Email signature & logo

Please add our [company logo](../../general/marketing/) to your email signature.

## Onboarding process

Welcome to the team!

Before you join, your manager should create an onboarding ticket in GitLab. To
do this:

1. Go to [our handbook project](https://gitlab.com/synura/handbook/-/issues/new)
2. Create a Title of "Onboarding -- employee name"
3. Select Choose a template next to "Description"
4. Select the New Teammate Onboarding template (creating this is still TBD).
   This template has todos for the new teammate, for their manager, and to help
   set up the proper administrative needs.
5. Schedule a daily check-in for each day of the first two weeks. Add this
   onboarding 1:1 template (document TBD) as the notes for the agenda

### Schedule

Your first week will be "company onboarding", led by CEO and PeopleOps/or
designee, and your second week will be "role onboarding," led by your manager.

In week 1, we will cover these topics:

1. [Company direction](../../general/direction/)
2. [Values](../../general/values/)
3. [Communications](../../general/communication/)
4. [Meeting norms](../../general/meetings/)
5. Handbook-first process and training on its first use cases
6. What's novel / uncommon about Synura practices
7. Go through expectations of your workstation setup, review Finance processes &
   People Group - Led by PeopleOps, make sure you complete the Emergency Contact
   Form (template TBD).
8. Meet the Team - _Welcome to Synura - All Team Members Call_

You can also read about the [tools we use](../../general/tools/) and how we use
them.

In week 2, you will work with your manager to:

1. Review our company goals and draft personal goals for yourself for 6 weeks, 3
   months, and 6 months if it's helpful
2. Define your personal goals
3. Understand your role and responsibilities

### Administrative needs

With each teammate we welcome, we learn a bit more about how to bring people
onboard. As we learn, we update the template that we use, which is still TBD to
be created. Onboarding applies to full-time team members as well as part-time
team members.

### Relocation expenses

If you’re moving for work, Synura will pay relocation costs subject to
pre-approval on the amount by your manager.

### New teammate welcome call

When a new teammate joins, we take the opportunity for the whole company to come
together and welcome them to the team. Like the Biweekly Team call, this call is
different than all other Synura meetings because it is mostly focused on the
team we're building, not our product or programs or customer relationships. We
do this ritual to celebrate the growth of our team, to reflect back on how we've
grown over time and how every single new person has changed the trajectory of
our company, to form new relationships among ourselves and to help everyone know
the newest member of the Synura team.

- We use our all-team notes document (document TBD)
- We go through in order of Start Date from our team milestones (document TBD)
- Each person introduces themselves, explains their role, and shares some tidbit
  about themselves (this changes for each meeting).
- Each person then calls on the next teammate on the list.
